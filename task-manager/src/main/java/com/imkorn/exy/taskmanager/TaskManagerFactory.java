package com.imkorn.exy.taskmanager;

import androidx.annotation.AnyThread;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.concurrent.locks.ReentrantReadWriteLock;

@AnyThread
public abstract class TaskManagerFactory {

	public void setTaskManagerInterceptor(@Nullable final TaskManagerInterceptor taskManagerInterceptor) {
		this.taskManagerInterceptor = taskManagerInterceptor;
	}

	@NonNull
    public final TaskManager getTaskManager() throws IllegalStateException {
        if(released) throw new IllegalStateException(ERROR_FACTORY_RELEASED);

        final ReentrantReadWriteLock.ReadLock readLock = lock.readLock();
        if(readLock.tryLock()) {
			if(released) {
				readLock.unlock();
				throw new IllegalStateException(ERROR_FACTORY_RELEASED);
			}

            final TaskManager taskManager = getTaskManagerInternal();
			final TaskManagerInterceptor taskManagerInterceptor = this.taskManagerInterceptor;
			if(taskManagerInterceptor != null) taskManagerInterceptor.onIntercept(taskManager);
            readLock.unlock();
            return taskManager;
        } else {
            throw new IllegalStateException(ERROR_FACTORY_RELEASED);
        }
    }

    public final void release() {
        if(released) return;

        final ReentrantReadWriteLock.WriteLock writeLock = lock.writeLock();
        writeLock.lock();
        if(!released) {
            released = true;
            releaseInternal();
			taskManagerInterceptor = null;
		}
        writeLock.unlock();
    }

    public final boolean isReleased() {
        return released;
    }

    protected abstract TaskManager getTaskManagerInternal();
    protected abstract void releaseInternal();

    private volatile boolean released = false;
    private volatile TaskManagerInterceptor taskManagerInterceptor = null;
    private final ReentrantReadWriteLock lock = new ReentrantReadWriteLock();
    private static final String ERROR_FACTORY_RELEASED = "Task factory released";
}
