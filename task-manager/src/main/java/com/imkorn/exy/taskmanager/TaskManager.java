package com.imkorn.exy.taskmanager;

import androidx.annotation.AnyThread;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

@AnyThread
public interface TaskManager {
    void postRunnable(@NonNull Runnable runnable);
    void postRunnable(@NonNull Runnable runnable, long delayMillis);
    void removeRunnable(@Nullable Runnable runnable);

    void postTask(int type, int arg1, int arg2, @Nullable Object argObj);
    void postTask(int type, int arg1, int arg2);
    void postTask(int type, @Nullable Object argObj);
    void postTask(int type);

    void postTaskAtFront(int type, int arg1, int arg2, @Nullable Object argObj);
    void postTaskAtFront(int type, int arg1, int arg2);
    void postTaskAtFront(int type, @Nullable Object argObj);
    void postTaskAtFront(int type);

    @NonNull
    Thread getThread();

    void setTaskHandler(@Nullable TaskHandler taskHandler);
    void clearAll();
}
