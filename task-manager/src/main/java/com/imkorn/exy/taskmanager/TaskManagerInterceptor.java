package com.imkorn.exy.taskmanager;

import androidx.annotation.NonNull;

public interface TaskManagerInterceptor {
	void onIntercept(@NonNull final TaskManager taskManager);
}
