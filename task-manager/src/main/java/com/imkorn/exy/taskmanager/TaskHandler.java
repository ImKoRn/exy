package com.imkorn.exy.taskmanager;

import androidx.annotation.Nullable;
import androidx.annotation.WorkerThread;

public interface TaskHandler {
    @WorkerThread
    void handle(int type, int arg1, int arg2, @Nullable Object argObj);
}
