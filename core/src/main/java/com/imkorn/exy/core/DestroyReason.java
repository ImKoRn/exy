package com.imkorn.exy.core;

import com.imkorn.exy.core.context.NodeContext;

public enum DestroyReason {
    /**
     * {@link NodeContext#destroy(boolean)} invoked
     * */
    SELF,
    /**
     * Node route destroyed
     * */
    ROUTE_DESTROY,
    /**
     * Unhandled exception in method
     * */
    FAILURE,
    /**
     * Parent destroying
     * */
    PARENT_DESTROY
}
