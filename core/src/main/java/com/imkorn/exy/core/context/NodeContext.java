package com.imkorn.exy.core.context;

import androidx.annotation.AnyThread;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.imkorn.exy.core.State;
import com.imkorn.exy.mapping.Routes;
import com.imkorn.exy.mapping.Api;

/**
 * Execution context
 */
@AnyThread
public interface NodeContext<ApiType extends Api, RoutesType extends Routes> {
    @NonNull
    ApiType getApi();

    @NonNull
    RoutesType getRoutes() throws ClassCastException;

    boolean isRouter();

    void destroy(boolean force);

    @NonNull
	State getState();

    void post(@NonNull Runnable runnable);

    void remove(@Nullable Runnable runnable);

    @NonNull
    Thread getThread();
}
