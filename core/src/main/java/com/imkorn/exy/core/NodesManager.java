package com.imkorn.exy.core;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.imkorn.exy.taskmanager.TaskManagerFactory;
import com.imkorn.exy.core.context.NodeContext;
import com.imkorn.exy.core.exceptions.DestroyedNodeException;
import com.imkorn.exy.core.exceptions.ReleasedResourcesAccessException;
import com.imkorn.exy.core.exceptions.RouteRegistrationNodeException;
import com.imkorn.exy.mapping.Mapper;
import com.imkorn.exy.mapping.Router;
import com.imkorn.exy.mapping.Routes;
import com.imkorn.exy.mapping.Api;

/**
 * Manager for creating child {@link Node}s
 */
public interface NodesManager {


    /**
     * Creates new dynamic {@link Node} without {@link Routes}.
     *
     * @param lifecycle lifecycle type of created {@link Node}
     * @param mapper lifecycle mapping rules
     * @param taskManagerFactory if null parent {@link TaskManagerFactory} will be used
     *
     * @throws DestroyedNodeException                caused when this method called when {@link Node} not active
     * @throws ReleasedResourcesAccessException this {@link NodesManager} {@link Node} was destroyed and released
     * @see #createNode
     * @see #createRouterNode
     * @see NodeContext#getState()
     */
    @NonNull
    <LifecycleType extends Lifecycle<SelfApiType, ?>, PublicApiType extends Api, SelfApiType extends Api>
    Node<PublicApiType, Routes> createNode(@NonNull final Class<? extends LifecycleType> lifecycle,
                                           @NonNull final Mapper<LifecycleType, PublicApiType, ?, SelfApiType> mapper,
                                           @Nullable final TaskManagerFactory taskManagerFactory)
    throws DestroyedNodeException, ReleasedResourcesAccessException;
    /**
     * Creates new dynamic {@link Node} without {@link Routes} using parent {@link TaskManagerFactory}.
     * <p>
     * Same as {@link NodesManager#createNode(Class, Mapper, TaskManagerFactory)} with null {@link TaskManagerFactory}
     * </p>
     *
     * @param lifecycle lifecycle type of created {@link Node}
     * @param mapper lifecycle mapping rules
     *
     * @throws DestroyedNodeException                caused when this method called when {@link Node} not active
     * @throws ReleasedResourcesAccessException this {@link NodesManager} {@link Node} was destroyed and released
     * @see #createNode
     * @see #createRouterNode
     * @see NodeContext#getState()
     */
    @NonNull
    <LifecycleType extends Lifecycle<SelfApiType, ?>, PublicApiType extends Api, SelfApiType extends Api>
    Node<PublicApiType, Routes> createNode(@NonNull final Class<? extends LifecycleType> lifecycle,
                                           @NonNull final Mapper<LifecycleType, PublicApiType, ?, SelfApiType> mapper)
    throws DestroyedNodeException, ReleasedResourcesAccessException;

    /**
     * Creates new dynamic {@link Node} with {@link Routes}.
     *
     * @param lifecycle lifecycle type of created {@link Node}
     * @param mapper lifecycle mapping rules and {@link Routes} description
     * @param taskManagerFactory if null parent {@link TaskManagerFactory} will be used
     *
     * @throws DestroyedNodeException                caused when this method called when {@link Node} not active
     * @throws ReleasedResourcesAccessException this {@link NodesManager} {@link Node} was destroyed and released
     * @see #createNode
     * @see #createRouterNode
     * @see NodeContext#getState()
     */
    @NonNull
    <LifecycleType extends Lifecycle<SelfApiType, RoutesType>,
     PublicApiType extends Api, SelfApiType extends Api,
     RoutesType extends Routes,
     MapperType extends Mapper<LifecycleType, PublicApiType, ?, SelfApiType> & Router<RoutesType>>
    Node<PublicApiType, RoutesType> createRouterNode(@NonNull Class<? extends LifecycleType> lifecycle,
                                                     @NonNull MapperType mapper,
                                                     @Nullable final TaskManagerFactory taskManagerFactory)
    throws DestroyedNodeException, ReleasedResourcesAccessException;
    /**
     * Creates new dynamic {@link Node} with {@link Routes} using parent {@link TaskManagerFactory}.
     * <p>
     * Same as {@link NodesManager#createRouterNode(Class, Mapper, TaskManagerFactory)} with null {@link TaskManagerFactory}
     * </p>
     *
     * @param lifecycle lifecycle type of created {@link Node}
     * @param mapper lifecycle mapping rules
     *
     * @throws DestroyedNodeException                caused when this method called when {@link Node} not active
     * @throws ReleasedResourcesAccessException this {@link NodesManager} {@link Node} was destroyed and released
     * @see #createNode
     * @see #createRouterNode
     * @see NodeContext#getState()
     */
    @NonNull
    <LifecycleType extends Lifecycle<SelfApiType, RoutesType>,
     PublicApiType extends Api, SelfApiType extends Api,
     RoutesType extends Routes,
     MapperType extends Mapper<LifecycleType, PublicApiType, ?, SelfApiType> & Router<RoutesType>>
    Node<PublicApiType, RoutesType> createRouterNode(@NonNull Class<? extends LifecycleType> lifecycle,
                                                     @NonNull MapperType mapper)
    throws DestroyedNodeException, ReleasedResourcesAccessException;

    /**
     * Registers {@link Node} for {@link Routes} without {@link Routes}.
     *
     * @param routeId route unique id
     * @param lifecycle lifecycle type of registered {@link Node} route
     * @param mapper lifecycle mapping rules
     * @param taskManagerFactory if null parent {@link TaskManagerFactory} will be used
     *
     * @throws RouteRegistrationNodeException caused when this method called when {@link Node} not active
     * @throws ReleasedResourcesAccessException this {@link NodesManager} {@link Node} was destroyed and released
     * @see #registerRoute(int, Class, Mapper)
     * @see #registerRouterRoute(int, Class, Mapper)
     * @see #registerRouterRoute(int, Class, Mapper, TaskManagerFactory)
     * @see NodeContext#getState()
     */
    @NonNull
    <LifecycleType extends Lifecycle<SelfApiType, ?>,
     PublicApiType extends Api, SelfApiType extends Api>
    Node<PublicApiType, Routes> registerRoute(final int routeId,
                                              @NonNull final Class<? extends LifecycleType> lifecycle,
                                           @NonNull final Mapper<LifecycleType, PublicApiType, ?, SelfApiType> mapper,
                                           @Nullable final TaskManagerFactory taskManagerFactory)
    throws RouteRegistrationNodeException, ReleasedResourcesAccessException;
    /**
     * Registers {@link Node} for {@link Routes} without {@link Routes} using parent {@link TaskManagerFactory}.
     * <p>
     * Same as {@link NodesManager#createNode(Class, Mapper, TaskManagerFactory)} with null {@link TaskManagerFactory}
     * </p>
     *
     * @param routeId route unique id
     * @param lifecycle lifecycle type of registered {@link Node} route
     * @param mapper lifecycle mapping rules
     *
     * @throws RouteRegistrationNodeException caused when this method called when {@link Node} not active
     * @throws ReleasedResourcesAccessException this {@link NodesManager} {@link Node} was destroyed and released
     * @see #registerRoute(int, Class, Mapper, TaskManagerFactory)
     * @see #registerRouterRoute(int, Class, Mapper)
     * @see #registerRouterRoute(int, Class, Mapper, TaskManagerFactory)
     * @see NodeContext#getState()
     */
    @NonNull
    <LifecycleType extends Lifecycle<SelfApiType, ?>,
     PublicApiType extends Api, SelfApiType extends Api>
    Node<PublicApiType, Routes> registerRoute(final int routeId,
                                              @NonNull final Class<? extends LifecycleType> lifecycle,
                                           @NonNull final Mapper<LifecycleType, PublicApiType, ?, SelfApiType> mapper)
    throws DestroyedNodeException, ReleasedResourcesAccessException;

    /**
     * Registers {@link Node} for {@link Routes} with {@link Routes}.
     *
     * @param routeId route unique id
     * @param lifecycle lifecycle type of registered {@link Node} route
     * @param mapper lifecycle mapping rules and {@link Routes} description
     * @param taskManagerFactory if null parent {@link TaskManagerFactory} will be used
     *
     * @throws RouteRegistrationNodeException                caused when this method called when {@link Node} not active
     * @throws ReleasedResourcesAccessException this {@link NodesManager} {@link Node} was destroyed and released
     * @see #registerRoute(int, Class, Mapper)
     * @see #registerRoute(int, Class, Mapper, TaskManagerFactory)
     * @see #registerRouterRoute(int, Class, Mapper)
     * @see NodeContext#getState()
     */
    @NonNull
    <LifecycleType extends Lifecycle<SelfApiType, RoutesType>,
     PublicApiType extends Api,
     SelfApiType extends Api,
     RoutesType extends Routes,
     MapperType extends Mapper<LifecycleType, PublicApiType, ?, SelfApiType> & Router<RoutesType>>
    Node<PublicApiType, RoutesType> registerRouterRoute(final int routeId,
                                                        @NonNull Class<? extends LifecycleType> lifecycle,
                                                     @NonNull MapperType mapper,
                                                     @Nullable final TaskManagerFactory taskManagerFactory)
    throws RouteRegistrationNodeException, ReleasedResourcesAccessException;
    /**
     * Registers {@link Node} for {@link Routes} with {@link Routes} using parent {@link TaskManagerFactory}.
     * <p>
     * Same as {@link NodesManager#registerRouterRoute(int, Class, Mapper, TaskManagerFactory)} with null {@link TaskManagerFactory}
     * </p>
     *
     * @param routeId route unique id
     * @param lifecycle lifecycle type of registered {@link Node} route
     * @param mapper lifecycle mapping rules
     *
     * @throws RouteRegistrationNodeException caused when this method called when {@link Node} is not creating
     * @throws ReleasedResourcesAccessException this {@link NodesManager} {@link Node} was destroyed and released
     * @see #registerRoute(int, Class, Mapper)
     * @see #registerRoute(int, Class, Mapper, TaskManagerFactory)
     * @see #registerRouterRoute(int, Class, Mapper, TaskManagerFactory)
     * @see NodeContext#getState()
     */
    @NonNull
    <LifecycleType extends Lifecycle<SelfApiType, RoutesType>,
     PublicApiType extends Api,
     SelfApiType extends Api,
     RoutesType extends Routes,
     MapperType extends Mapper<LifecycleType, PublicApiType, ?, SelfApiType> & Router<RoutesType>>
    Node<PublicApiType, RoutesType> registerRouterRoute(final int routeId,
                                                        @NonNull Class<? extends LifecycleType> lifecycle,
                                                        @NonNull MapperType mapper)
    throws RouteRegistrationNodeException, ReleasedResourcesAccessException;

}