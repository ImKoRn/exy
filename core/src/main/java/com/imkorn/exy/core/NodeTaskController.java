package com.imkorn.exy.core;

import androidx.annotation.AnyThread;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.WorkerThread;

import com.imkorn.exy.taskmanager.TaskHandler;
import com.imkorn.exy.taskmanager.TaskManager;


class NodeTaskController {

    @AnyThread
    public void postRunnable(@NonNull Runnable runnable) {
        taskManager.postRunnable(runnable);
    }

    @AnyThread
    public void removeRunnable(@Nullable Runnable runnable)
    {
        taskManager.removeRunnable(runnable);
    }

    @AnyThread
    public void postMethod(final int methodId, @Nullable final Object args) {
        taskManager.postTask(NodeTask.TASK_INVOKE, methodId, 0 /*not used*/, args);
    }

    @AnyThread
    public void postRouteMethod(final int routeId, final int methodId, @Nullable final Object args) {
        taskManager.postTask(NodeTask.TASK_REDIRECT_INVOKE, routeId, methodId, args);
    }

    @AnyThread
    public void postDestroy(final boolean force, @NonNull DestroyReason destroyReason) {
        if(force) {
            taskManager.postTaskAtFront(NodeTask.TASK_DESTROY, 1, destroyReason.ordinal());
        } else {
            taskManager.postTask(NodeTask.TASK_DESTROY, 0, destroyReason.ordinal());
        }
    }

    @AnyThread
    public void postChildDestroyed(@NonNull final Node<?, ?> node) {
        taskManager.postTask(NodeTask.TASK_CHILD_DESTROYED, node);
    }

    public Thread getThread() {
        return taskManager.getThread();
    }

    NodeTaskController(@NonNull final Node<?, ?> node, @NonNull final TaskManager taskManager) {
        this.node = node;
        this.taskManager = taskManager;
    }

    /**
     * Controller internal state initializing
     * */
    @WorkerThread
    void init() {
        taskManager.setTaskHandler(taskHandler);
        taskManager.postTask(NodeTask.TASK_CREATE);
    }
    /**
     * Controller internal state release
     * */
    @WorkerThread
    void release() {
        taskManager.setTaskHandler(null);
    }

    @WorkerThread
    private void onCreateLifecycle() {
        node.onCreateLifecycle();
    }

    @WorkerThread
    private void onStartDestroy(final int force, final int destroyReason) {
        node.onStartDestroy(force == 1, DestroyReason.values()[destroyReason]);
    }

    @WorkerThread
    private void onChildDestroyed(final Object rawNode) {
        node.onChildDestroyed((Node<?, ?>) rawNode);
    }

    @SuppressWarnings("unchecked")
    @WorkerThread
    private void onInvokeMethod(final int methodId, @Nullable final Object args) {
        node.onInvokeMethod(methodId, args);
    }

    @WorkerThread
    private void onRedirectInvoke(final int routeId, final int methodId, @Nullable final Object args) {
        node.onRedirectInvoke(routeId, methodId, args);
    }

    private final TaskManager taskManager;
    private final Node<?, ?> node;

    private final TaskHandler taskHandler = (taskId, arg1, arg2, argObj) -> {
        switch(taskId) {
            // Lifecycle
            case NodeTask.TASK_CREATE:          onCreateLifecycle();                     break;
            case NodeTask.TASK_DESTROY:         onStartDestroy(arg1, arg2);              break;
            case NodeTask.TASK_CHILD_DESTROYED: onChildDestroyed(argObj);                break;

            // Messages
            case NodeTask.TASK_INVOKE:          onInvokeMethod(arg1, argObj);            break;
            case NodeTask.TASK_REDIRECT_INVOKE: onRedirectInvoke(arg1, arg2, argObj);    break;
        }
    };
}
