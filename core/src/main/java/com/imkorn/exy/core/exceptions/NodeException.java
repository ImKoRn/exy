package com.imkorn.exy.core.exceptions;

public class NodeException extends RuntimeException {
    public NodeException() {}

    public NodeException(final String message) {
        super(message);
    }

    public NodeException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public NodeException(final Throwable cause) {
        super(cause);
    }
}
