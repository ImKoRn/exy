package com.imkorn.exy.core.context;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.imkorn.exy.core.Node;
import com.imkorn.exy.core.State;
import com.imkorn.exy.mapping.Routes;
import com.imkorn.exy.mapping.Api;

/**
 * Node context
 */
public class NodeContextImpl<ApiType extends Api, RoutesType extends Routes> implements NodeContext<ApiType, RoutesType>
{

    @NonNull
    public static <ApiType extends Api, RoutesType extends Routes>
    NodeContextImpl<ApiType, RoutesType> create(@NonNull ApiType api, @NonNull Node<?, RoutesType> node) {
        return new NodeContextImpl<>(api, node);
    }

    @Override
    public void destroy(final boolean force) {
        node.destroy(force);
    }

    @NonNull
    @Override
    public State getState() {
        return node.getState();
    }

    @Override
    public void post(@NonNull Runnable runnable) {
        node.post(runnable);
    }

    @Override
    public void remove(@Nullable Runnable runnable) {
        node.remove(runnable);
    }

    @NonNull
    @Override
    public Thread getThread() {
        return node.getThread();
    }

    @NonNull
    @Override
    public ApiType getApi() {
        return api;
    }

    @NonNull
    @Override
    public RoutesType getRoutes() {
        return node.getRoutes();
    }

    @Override
    public boolean isRouter() {
        return node.isRouter();
    }

    private NodeContextImpl(@NonNull ApiType api, @NonNull Node<?, RoutesType> node) {
        this.api = api;
        this.node = node;
    }

    private final ApiType api;
    private final Node<?, RoutesType> node;
}
