package com.imkorn.exy.core;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.imkorn.exy.taskmanager.TaskManagerFactory;
import com.imkorn.exy.core.exceptions.DestroyedNodeException;
import com.imkorn.exy.core.exceptions.ReleasedResourcesAccessException;
import com.imkorn.exy.core.exceptions.RouteRegistrationNodeException;
import com.imkorn.exy.mapping.Mapper;
import com.imkorn.exy.mapping.Router;
import com.imkorn.exy.mapping.Routes;
import com.imkorn.exy.mapping.Api;

import java.lang.ref.WeakReference;

/**
 * Week reference to {@link NodesManager}
 */
final class NodesManagerRef implements NodesManager {

    @NonNull
    public static NodesManager create(@NonNull final NodesManager origin)
    throws ReleasedResourcesAccessException {
        return new NodesManagerRef(origin);
    }

    @NonNull
    @Override
    public <LifecycleType extends Lifecycle<SelfApiType, ?>,
            PublicApiType extends Api, SelfApiType extends Api>
    Node<PublicApiType, Routes> createNode(@NonNull final Class<? extends LifecycleType> lifecycle,
                                           @NonNull final Mapper<LifecycleType, PublicApiType, ?, SelfApiType> mapper,
                                           @Nullable final TaskManagerFactory taskManagerFactory)
    throws DestroyedNodeException, ReleasedResourcesAccessException {
        return getNodesManagerOrThrow().createNode(lifecycle, mapper, taskManagerFactory);
    }

    @NonNull
    @Override
    public <LifecycleType extends Lifecycle<SelfApiType, ?>,
            PublicApiType extends Api, SelfApiType extends Api>
    Node<PublicApiType, Routes> createNode(@NonNull final Class<? extends LifecycleType> lifecycle,
                                           @NonNull final Mapper<LifecycleType, PublicApiType, ?, SelfApiType> mapper)
    throws DestroyedNodeException, ReleasedResourcesAccessException {
        return getNodesManagerOrThrow().createNode(lifecycle, mapper, null);
    }

    @NonNull
    @Override
    public <LifecycleType extends Lifecycle<SelfApiType, RoutesType>,
            PublicApiType extends Api,
            SelfApiType extends Api,
            RoutesType extends Routes,
            MapperType extends Mapper<LifecycleType, PublicApiType, ?, SelfApiType> & Router<RoutesType>>
    Node<PublicApiType, RoutesType> createRouterNode(@NonNull final Class<? extends LifecycleType> lifecycle,
                                                     @NonNull final MapperType mapper,
                                                     @Nullable final TaskManagerFactory taskManagerFactory)
    throws DestroyedNodeException, ReleasedResourcesAccessException {
        return getNodesManagerOrThrow().createRouterNode(lifecycle, mapper, taskManagerFactory);
    }
    @NonNull
    @Override
    public <LifecycleType extends Lifecycle<SelfApiType, RoutesType>,
            PublicApiType extends Api,
            SelfApiType extends Api,
            RoutesType extends Routes,
            MapperType extends Mapper<LifecycleType, PublicApiType, ?, SelfApiType> & Router<RoutesType>>
    Node<PublicApiType, RoutesType> createRouterNode(@NonNull final Class<? extends LifecycleType> lifecycle,
                                                     @NonNull final MapperType mapper)
    throws DestroyedNodeException, ReleasedResourcesAccessException {
        return getNodesManagerOrThrow().createRouterNode(lifecycle, mapper, null);
    }

    @NonNull
    @Override
    public <LifecycleType extends Lifecycle<SelfApiType, ?>,
            PublicApiType extends Api, SelfApiType extends Api>
    Node<PublicApiType, Routes> registerRoute(int routeId,
                                              @NonNull Class<? extends LifecycleType> lifecycle,
                                              @NonNull Mapper<LifecycleType, PublicApiType, ?, SelfApiType> mapper,
                                              @Nullable TaskManagerFactory taskManagerFactory) throws RouteRegistrationNodeException, ReleasedResourcesAccessException {
        return getNodesManagerOrThrow().registerRoute(routeId, lifecycle, mapper, taskManagerFactory);
    }

    @NonNull
    @Override
    public <LifecycleType extends Lifecycle<SelfApiType, ?>,
            PublicApiType extends Api,
            SelfApiType extends Api>
    Node<PublicApiType, Routes> registerRoute(int routeId,
                                              @NonNull Class<? extends LifecycleType> lifecycle,
                                              @NonNull Mapper<LifecycleType, PublicApiType, ?, SelfApiType> mapper) throws DestroyedNodeException, ReleasedResourcesAccessException {
        return getNodesManagerOrThrow().registerRoute(routeId, lifecycle, mapper, null);
    }

    @NonNull
    @Override
    public <LifecycleType extends Lifecycle<SelfApiType, RoutesType>,
            PublicApiType extends Api,
            SelfApiType extends Api,
            RoutesType extends Routes,
            MapperType extends Mapper<LifecycleType, PublicApiType, ?, SelfApiType> & Router<RoutesType>>
    Node<PublicApiType, RoutesType> registerRouterRoute(int routeId,
                                                        @NonNull Class<? extends LifecycleType> lifecycle,
                                                        @NonNull MapperType mapper,
                                                        @Nullable TaskManagerFactory taskManagerFactory) throws RouteRegistrationNodeException, ReleasedResourcesAccessException {
        return getNodesManagerOrThrow().registerRouterRoute(routeId, lifecycle, mapper, taskManagerFactory);
    }

    @NonNull
    @Override
    public <LifecycleType extends Lifecycle<SelfApiType, RoutesType>,
            PublicApiType extends Api,
            SelfApiType extends Api,
            RoutesType extends Routes,
            MapperType extends Mapper<LifecycleType, PublicApiType, ?, SelfApiType> & Router<RoutesType>>
    Node<PublicApiType, RoutesType> registerRouterRoute(int routeId,
                                                        @NonNull Class<? extends LifecycleType> lifecycle,
                                                        @NonNull MapperType mapper) throws RouteRegistrationNodeException, ReleasedResourcesAccessException {
        return getNodesManagerOrThrow().registerRouterRoute(routeId, lifecycle, mapper, null);
    }

    @NonNull
    private NodesManager getNodesManagerOrThrow()
    throws ReleasedResourcesAccessException
    {
        final NodesManager nodesManager = originRef.get();
        if(nodesManager == null) {
            throw new ReleasedResourcesAccessException("Node containing this NodesManager was released");
        }
        return nodesManager;
    }

    private NodesManagerRef(@NonNull final NodesManager origin) {
        originRef = new WeakReference<>(origin);
    }

    private WeakReference<NodesManager> originRef;
}