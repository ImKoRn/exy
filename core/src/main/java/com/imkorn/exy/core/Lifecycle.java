package com.imkorn.exy.core;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.imkorn.exy.annotations.methods.DefAsync;
import com.imkorn.exy.core.context.NodeContext;
import com.imkorn.exy.mapping.Routes;
import com.imkorn.exy.mapping.Api;

/**
 * {@link Node} lifecycle with it's base events, all code executing in {@link Node} thread
 */
public abstract class Lifecycle<SelfApiType extends Api, RoutesType extends Routes> implements NodeContext<SelfApiType, RoutesType> {

    /**
     * <p> Calls when this lifecycle {@link Node} just created, before any other events or methods </p>
     * <p> Init all needed data here <p/>
     *
     * @see Lifecycle#onStartDestroy(DestroyReason)
     * @see Lifecycle#onFinishDestroy(DestroyReason)
     */
    public void onCreate() {
    }

    /**
     * <p>
     * Calls when this lifecycle {@link Node} start destroying, this can be caused by:
     * <ol>
     * <li>{@link #onError(int, Throwable)} returns false</li>
     * <li>{@link Node} removing this {@link Node}</li>
     * </ol>
     * </p>
     * <p> After this method all children {@link Node}s will be destroyed, when last child {@link Node} destroyed or no children will be called {@link #onFinishDestroy(DestroyReason)} </p>
     *
     * @see Lifecycle#onChildDestroyed(Node)
     * @see Lifecycle#onFinishDestroy(DestroyReason)
     */
    public void onStartDestroy(@NonNull DestroyReason destroyReason) {
    }

    /**
     * <p>
     * Calls when this {@link Node} finish destroying self, this cause when all routes and child nodes was destroyed
     * <p> Best place to free all resources that you want </p>
     *
     * @see Lifecycle#onStartDestroy(DestroyReason)
     * @see Lifecycle#onChildDestroyed(Node)
     */
    public void onFinishDestroy(@NonNull DestroyReason destroyReason) {
    }

    /**
     * <p> Calls when your method throws non expected {@link Throwable}, or failed method was defined as {@link DefAsync} </p>
     * <p> Try to handle throwable in this method if it's impossible return false, and this {@link Node} will be destroyed </p>
     *
     * @return true if throwable successfully processed, false otherwise
     * @see Lifecycle#onStartDestroy(DestroyReason)
     * @see Lifecycle#onChildDestroyed(Node)
     * @see Lifecycle#onFinishDestroy(DestroyReason)
     */
    public boolean onError(final int methodId, @NonNull final Throwable throwable) {
        return false;
    }

    /**
     * <p> Calls when child node or route was destroyed </p>
     * <p> Try to handle throwable in this method if it's impossible return false, and this {@link Node} will be destroyed </p>
     */
    public void onChildDestroyed(@NonNull final Node<?, ?> node) {
    }

    @Override
    public void destroy(final boolean force) {
        owner.destroy(force);
    }

    @Override
    @NonNull
    public State getState() {
        return owner.getState();
    }

    @Override
    public void post(@NonNull Runnable runnable) {
        owner.post(runnable);
    }

    @Override
    public void remove(@Nullable Runnable runnable) {
        owner.remove(runnable);
    }

    @NonNull
    @Override
    public SelfApiType getApi() {
        return api;
    }

    @NonNull
    @Override
    public RoutesType getRoutes() throws ClassCastException {
        return owner.getRoutes();
    }

    @Override
    public boolean isRouter() {
        return owner.isRouter();
    }

    @Override
    @NonNull
    public Thread getThread() {
        return owner.getThread();
    }

    @NonNull
    public NodesManager getNodesManager() {
        return nodesManager;
    }

    @Nullable
    @SuppressWarnings("unchecked")
    public <ParentApiType extends Api, ParentRoutesType extends Routes> NodeContext<ParentApiType, ParentRoutesType> getParent() {
        return (NodeContext<ParentApiType, ParentRoutesType>) parent;
    }

    final void init(@NonNull SelfApiType api,
                    @NonNull Node<?, RoutesType> owner,
                    @NonNull NodesManager nodesManager,
                    @Nullable NodeContext<?, ?> parent) {
        this.api = api;
        this.owner = owner;
        this.nodesManager = nodesManager;
        this.parent = parent;
        onCreate();
    }

    final void finishDestroy(@NonNull DestroyReason destroyReason) {
        onFinishDestroy(destroyReason);
        api = null;
        owner = null;
        nodesManager = null;
        parent = null;
    }

    private SelfApiType                api;
    private NodesManager               nodesManager;
    private NodeContext<?, RoutesType> owner;
    private NodeContext<?, ?>          parent;
}