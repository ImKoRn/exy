package com.imkorn.exy.core;

import androidx.annotation.AnyThread;
import androidx.annotation.CheckResult;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.imkorn.exy.taskmanager.TaskManager;
import com.imkorn.exy.taskmanager.TaskManagerFactory;
import com.imkorn.exy.core.context.NodeContext;
import com.imkorn.exy.core.context.NodeContextImpl;
import com.imkorn.exy.core.exceptions.ExpectedException;
import com.imkorn.exy.core.exceptions.NodeException;
import com.imkorn.exy.mapping.Mapper;
import com.imkorn.exy.mapping.Router;
import com.imkorn.exy.mapping.Routes;
import com.imkorn.exy.mapping.Api;
import com.imkorn.exy.mapping.ApiInterceptor;

import java.util.Objects;

public class Node<PublicApiType extends Api, RoutesType extends Routes> implements NodeContext<PublicApiType, RoutesType> {

    // ---------------------------------------------------------------------------------------------
    // ------------------------------------ Base Node creation -------------------------------------
    // ---------------------------------------------------------------------------------------------

    // ---------------------------------- Node creation --------------------------------------------

    @NonNull
    public static <
    LifecycleType extends Lifecycle<SelfApiType, ?>,
    PublicApiType extends Api,
    SelfApiType extends Api>
    Node<PublicApiType, Routes> newBaseNode(@NonNull final Class<? extends LifecycleType> lifecycleClazz,
                                            @NonNull final Mapper<LifecycleType, PublicApiType, ?, SelfApiType> mapper,
                                            @NonNull final TaskManagerFactory taskManagerFactory) {
        return new Node<>(lifecycleClazz, mapper, NullRouter.get(), taskManagerFactory);
    }

    // ------------------------------- Router Node creation ----------------------------------------

    @NonNull
    public static <
    LifecycleType extends Lifecycle<SelfApiType, RoutesType>,
    PublicApiType extends Api,
    SelfApiType extends Api,
    RoutesType extends Routes,
    MapperType extends Mapper<LifecycleType, PublicApiType, ?, SelfApiType> & Router<RoutesType>>
    Node<PublicApiType, RoutesType> newBaseRouterNode(@NonNull final Class<? extends LifecycleType> lifecycleClazz,
                                                      @NonNull final MapperType mapper,
                                                      @NonNull final TaskManagerFactory taskManagerFactory) {
        return new Node<>(lifecycleClazz, mapper, mapper, taskManagerFactory);
    }

    // ---------------------------------------------------------------------------------------------
    // --------------------------------- Node implementation ---------------------------------------
    // ---------------------------------------------------------------------------------------------

    @AnyThread
    @NonNull
    @Override
    public PublicApiType getApi() {
        return publicApi;
    }

    @AnyThread
    @Override
    @NonNull
    public RoutesType getRoutes() throws ClassCastException {
        return routes;
    }

    @Override
    @CheckResult
    public boolean isRouter() {
        return router;
    }

    @AnyThread
    @Override
    public void destroy(final boolean force) {
        taskController.postDestroy(force, DestroyReason.SELF);
    }

    @NonNull
    @AnyThread
    @Override
    @CheckResult
    public State getState() {
        return state;
    }

    @AnyThread
    @Override
    public void post(@NonNull Runnable runnable) {
        taskController.postRunnable(runnable);
    }

    @Override
    public void remove(@Nullable Runnable runnable) {
        taskController.removeRunnable(runnable);
    }

    @Override
    @NonNull
    @CheckResult
    public Thread getThread() {
        return taskController.getThread();
    }

    @Override
    @CheckResult
    public String toString() {
        return getClass().getSimpleName() + '[' +
        state.toString() + ',' +
        lifecycleClazz.getCanonicalName() + ',' +
        mapper.getClass().getCanonicalName() + ',' +
        taskController.getThread() + ']';
    }

    // -------------------------------------- Internal ---------------------------------------------

    @NonNull
    static <
    LifecycleType extends Lifecycle<SelfApiType, ?>,
    PublicApiType extends Api,
    SelfApiType extends Api>
    Node<PublicApiType, Routes> newNode(@NonNull final Node<?, ?> parent,
                                        @NonNull final Class<? extends LifecycleType> lifecycleClazz,
                                        @NonNull final Mapper<LifecycleType, PublicApiType, ?, SelfApiType> mapper,
                                        @Nullable final TaskManagerFactory taskManagerFactory) {
        Objects.requireNonNull(parent);
        return new Node<>(parent, lifecycleClazz, mapper, NullRouter.get(), taskManagerFactory != null?
                                                                            taskManagerFactory :
                                                                            parent.taskManagerFactory);
    }

    @NonNull
    static <
    LifecycleType extends Lifecycle<SelfApiType, RoutesType>,
    PublicApiType extends Api,
    SelfApiType extends Api,
    RoutesType extends Routes,
    MapperType extends Mapper<LifecycleType, PublicApiType, ?, SelfApiType> & Router<RoutesType>>
    Node<PublicApiType, RoutesType> newRouterNode(@NonNull final Node<?, ?> parent,
                                                  @NonNull final Class<? extends LifecycleType> lifecycleClazz,
                                                  @NonNull final MapperType mapper,
                                                  @Nullable final TaskManagerFactory taskManagerFactory) {
        Objects.requireNonNull(parent);
        return new Node<>(parent, lifecycleClazz, mapper, mapper, taskManagerFactory != null?
                                                                  taskManagerFactory :
                                                                  parent.taskManagerFactory);
    }

    /**
     * Creates lifecycle of this {@link Node}
     * Always invoked first
     */
    @SuppressWarnings("unchecked")
    void onCreateLifecycle() {
        checkLifecycle(true);

        SetStatus(State.CREATING);
        try {
            lifecycle = lifecycleClazz.newInstance();
        } catch(Throwable throwable) {
            SetStatus(State.DESTROYED);
            final String name = lifecycleClazz.getName();
            throw new NodeException("Node Lifecycle instantiation error.\n" +
                                    "Please check this rules:\n" +
                                    "  1) " + name + " must have public empty constructor\n" +
                                    "  2) " + name + " must be non abstract\n",
                                    throwable);
        }
        lifecycle.init(selfApi,
                       this,
                       NodesManagerRef.create(nodesManager),
                       parent != null ? parent.childContext : null);
        nodesManager.checkAllRoutesRegistered();
        SetStatus(State.CREATED);

        if(destroyAfterCreation) {
            onStartDestroy(true, destroyReason);
        }
    }

    void onInvokeMethod(final int methodId, @Nullable final Object args) {
        if(!state.isActive()) return;
        checkLifecycle(false);

        try {
            mapper.map(lifecycle, methodId, args);
        } catch(ExpectedException exception) {
            // exception expected - ignoring
        } catch(Throwable throwable) {
            if(lifecycle.onError(methodId, throwable)) return;
            onStartDestroy(true, DestroyReason.FAILURE);
        }
    }

    @SuppressWarnings("unchecked")
    void onRedirectInvoke(int routeId, int methodId, @Nullable Object rawPayload) {
        if(!state.isActive()) return;
        checkLifecycle(false);

        final Node<?, ?> route = nodesManager.getRawRoute(routeId);

        if(route == null) throw new NodeException("Route not found");

        if(route.state.isActive()) {
            route.taskController.postMethod(methodId, rawPayload);
        }
    }

    @SuppressWarnings("unchecked")
    void onChildDestroyed(@NonNull final Node<?, ?> node) {
        checkLifecycle(false);

        final boolean isRoute = nodesManager.delete(node);
        lifecycle.onChildDestroyed(node);

        if(state == State.DESTROYING) {
            if(nodesManager.hasNodes()) return;
            // Finish node destroying
            onFinishDestroy();
        }
        else if(isRoute) {
            onStartDestroy(true, DestroyReason.ROUTE_DESTROY);
        }
    }

    void onStartDestroy(boolean force, @NonNull DestroyReason destroyReason) {
        if(!state.isActive()) return;

        if(state == State.IDLE) {
            if(!destroyAfterCreation)
            {
                this.destroyReason = destroyReason;
                destroyAfterCreation = true;
            }
            return;
        }

        checkLifecycle(false);

        SetStatus(State.DESTROYING);
        this.destroyReason = destroyReason;

        if(nodesManager.hasNodes()) {
            for(final Node<?, ?> node : nodesManager) {
                node.taskController.postDestroy(force, DestroyReason.PARENT_DESTROY);
            }
            lifecycle.onStartDestroy(destroyReason);
        } else {
            lifecycle.onStartDestroy(destroyReason);
            onFinishDestroy();
        }
    }


    private Node(@NonNull final Class<?> lifecycleClazz,
                 @NonNull final Mapper<?, ?, ?, ?> mapper,
                 @NonNull final Router<RoutesType> router,
                 @NonNull final TaskManagerFactory taskManagerFactory) {
        this(null, lifecycleClazz, mapper, router, taskManagerFactory);
    }

    @SuppressWarnings("unchecked")
    private Node(@Nullable final Node<?, ?> parent,
                 @NonNull final Class<?> lifecycleClazz,
                 @NonNull final Mapper<?, ?, ?, ?> mapper,
                 @NonNull final Router<RoutesType> router,
                 @NonNull final TaskManagerFactory taskManagerFactory) {
        Objects.requireNonNull(lifecycleClazz);
        Objects.requireNonNull(mapper);
        Objects.requireNonNull(router);
        Objects.requireNonNull(taskManagerFactory);

        this.parent = parent;
        this.lifecycleClazz = (Class<Lifecycle>) lifecycleClazz;
        this.mapper = (Mapper<Lifecycle, PublicApiType, ?, ?>) mapper;
        this.taskManagerFactory = taskManagerFactory;

        final NodeApiInterceptor interceptor = new NodeApiInterceptor();

        this.router = NullRouter.get() != router;
        this.routes = router.createRoutes(interceptor);
        this.publicApi = this.mapper.createPublicApi(THIS, interceptor);
        this.selfApi = this.mapper.createSelfApi(THIS, interceptor);
        final Api childApi = this.mapper.createChildApi(THIS, interceptor);
        this.childContext = NodeContextImpl.create(childApi, this);
        this.nodesManager = new InternalNodesManagerImpl(this, routes);

        final TaskManager taskManager = taskManagerFactory.getTaskManager();
        this.taskController = new NodeTaskController(this, taskManager);
        this.taskController.init();
    }

    private void onFinishDestroy() {
        checkLifecycle(false);

        SetStatus(State.DESTROYED);

        lifecycle.finishDestroy(destroyReason);
        lifecycle = null;
        taskController.release();

        // Notify parent
        if(parent != null) {
            parent.taskController.postChildDestroyed(this);
        }
    }

    private void SetStatus(@NonNull final State state)
    {
        if(this.state == state) return;
        this.state = state;
    }

    private void checkLifecycle(final boolean released) {
        if((lifecycle == null) != released) throw new NodeException("Lifecycle state invalid");
    }

    // ---------------------------------------------------------------------------------------------
    // ------------------------- Private Annotations/Interfaces/Classes ----------------------------
    // ---------------------------------------------------------------------------------------------

    private final class NodeApiInterceptor implements ApiInterceptor {
        @Override
        public void invoke(final int apiId, final int methodId, @Nullable final Object args) {
            if(!state.isActive()) return;

            if(apiId == THIS)
            {
                taskController.postMethod(methodId, args);
            }
            else
            {
                taskController.postRouteMethod(apiId, methodId, args);
            }
        }
    }

    // ---------------------------------------------------------------------------------------------
    // -------------------------------------- Fields -----------------------------------------------
    // ---------------------------------------------------------------------------------------------

    // Public
    private @NonNull final PublicApiType publicApi;
    private @NonNull final RoutesType routes;
    private @NonNull final Api selfApi;
    private final boolean router;

    private volatile State state = State.IDLE;

    // Internal
    private @NonNull  final Class<Lifecycle>           lifecycleClazz;
    private @NonNull  final Mapper<Lifecycle, PublicApiType, ?, ?> mapper;
    private @NonNull  final NodeContext<?, RoutesType> childContext;
    private @Nullable final Node<?, ?>                 parent;
    private @NonNull  final InternalNodesManager       nodesManager;
    private @NonNull  final NodeTaskController         taskController;
    private @NonNull  final TaskManagerFactory         taskManagerFactory;
    private Lifecycle lifecycle = null;

    private boolean destroyAfterCreation = false;
    private DestroyReason destroyReason;

    private static final int THIS = -1;
}