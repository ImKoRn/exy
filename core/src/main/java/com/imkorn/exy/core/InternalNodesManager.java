package com.imkorn.exy.core;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.imkorn.exy.core.exceptions.RouteRegistrationNodeException;

interface InternalNodesManager extends NodesManager, Iterable<Node<?, ?>> {
    @Nullable
    Node<?, ?> getRawRoute(int routeId);
    boolean delete(@NonNull Node<?, ?> node);
    void checkAllRoutesRegistered() throws RouteRegistrationNodeException;
    boolean hasNodes();
}