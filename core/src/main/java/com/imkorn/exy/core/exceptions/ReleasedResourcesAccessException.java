package com.imkorn.exy.core.exceptions;

public class ReleasedResourcesAccessException extends NodeException {
    public ReleasedResourcesAccessException() {}

    public ReleasedResourcesAccessException(final String message) {
        super(message);
    }

    public ReleasedResourcesAccessException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public ReleasedResourcesAccessException(final Throwable cause) {
        super(cause);
    }
}
