package com.imkorn.exy.core.exceptions;

/**
 *
 */
public class RouteRegistrationNodeException extends NodeException {
    public RouteRegistrationNodeException(String message) {
        super(message);
        printStackTrace();
    }
}
