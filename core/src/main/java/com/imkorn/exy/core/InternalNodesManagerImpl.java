package com.imkorn.exy.core;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.imkorn.exy.taskmanager.TaskManagerFactory;
import com.imkorn.exy.core.exceptions.DestroyedNodeException;
import com.imkorn.exy.core.exceptions.RouteRegistrationNodeException;
import com.imkorn.exy.mapping.Mapper;
import com.imkorn.exy.mapping.Router;
import com.imkorn.exy.mapping.Routes;
import com.imkorn.exy.mapping.Api;

import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

final class InternalNodesManagerImpl implements InternalNodesManager {

    @NonNull
    @Override
    public
    <LifecycleType extends Lifecycle<SelfApiType, ?>,
    PublicApiType extends Api, SelfApiType extends Api>
    Node<PublicApiType, Routes> createNode(@NonNull final Class<? extends LifecycleType> lifecycle,
                                               @NonNull final Mapper<LifecycleType, PublicApiType, ?, SelfApiType> mapper,
                                               @Nullable final TaskManagerFactory taskManagerFactory)
    throws DestroyedNodeException {
        checkDestroyState();
        final Node<PublicApiType, Routes> node = Node.newNode(owner, lifecycle, mapper, taskManagerFactory);
        nodesMap.put(node, null);
        return node;
    }

    @NonNull
    @Override
    public
    <LifecycleType extends Lifecycle<SelfApiType, ?>,
    PublicApiType extends Api, SelfApiType extends Api>
    Node<PublicApiType, Routes> createNode(@NonNull final Class<? extends LifecycleType> lifecycle,
                                           @NonNull final Mapper<LifecycleType, PublicApiType, ?, SelfApiType> mapper)
    throws DestroyedNodeException {
        return createNode(lifecycle, mapper, null);
    }

    @NonNull
    @Override
    public
    <LifecycleType extends Lifecycle<SelfApiType, RoutesType>,
    PublicApiType extends Api,
    SelfApiType extends Api,
    RoutesType extends Routes,
    MapperType extends Mapper<LifecycleType, PublicApiType, ?, SelfApiType> & Router<RoutesType>>
    Node<PublicApiType, RoutesType> createRouterNode(@NonNull final Class<? extends LifecycleType> lifecycle,
                                                             @NonNull final MapperType mapper,
                                                             @Nullable final TaskManagerFactory taskManagerFactory)
    throws DestroyedNodeException {
        checkDestroyState();
        final Node<PublicApiType, RoutesType> node = Node.newRouterNode(owner, lifecycle, mapper, taskManagerFactory);
        nodesMap.put(node, null);
        return node;
    }

    @NonNull
    @Override
    public
    <LifecycleType extends Lifecycle<SelfApiType, RoutesType>,
    PublicApiType extends Api,
    SelfApiType extends Api,
    RoutesType extends Routes,
    MapperType extends Mapper<LifecycleType, PublicApiType, ?, SelfApiType> & Router<RoutesType>>
    Node<PublicApiType, RoutesType> createRouterNode(@NonNull final Class<? extends LifecycleType> lifecycle,
                                                     @NonNull final MapperType mapper)
    throws DestroyedNodeException {
        return createRouterNode(lifecycle, mapper, null);
    }

    @NonNull
    @Override
    public
    <LifecycleType extends Lifecycle<SelfApiType, ?>,
    PublicApiType extends Api, SelfApiType extends Api>
    Node<PublicApiType, Routes> registerRoute(final int routeId,
                                                  @NonNull final Class<? extends LifecycleType> lifecycle,
                                                  @NonNull final Mapper<LifecycleType, PublicApiType, ?, SelfApiType> mapper,
                                                  @Nullable final TaskManagerFactory taskManagerFactory)
    throws RouteRegistrationNodeException {
        checkCreatingState();
        checkRouteNotRegistered(routeId);
        checkRouteValid(routeId, mapper.getClass());

        final Node<PublicApiType, Routes> route = Node.newNode(owner, lifecycle, mapper, taskManagerFactory);
        nodesMap.put(route, routeId);
        routesMap.put(routeId, route);
        return route;
    }

    @NonNull
    @Override
    public
    <LifecycleType extends Lifecycle<SelfApiType, ?>,
    PublicApiType extends Api, SelfApiType extends Api>
    Node<PublicApiType, Routes> registerRoute(final int routeId,
                                              @NonNull final Class<? extends LifecycleType> lifecycle,
                                              @NonNull final Mapper<LifecycleType, PublicApiType, ?, SelfApiType> mapper)
    throws RouteRegistrationNodeException {
        return registerRoute(routeId, lifecycle, mapper, null);
    }

    @NonNull
    @Override
    public
    <LifecycleType extends Lifecycle<SelfApiType, RoutesType>,
    PublicApiType extends Api,
    SelfApiType extends Api,
    RoutesType extends Routes,
    MapperType extends Mapper<LifecycleType, PublicApiType, ?, SelfApiType> & Router<RoutesType>>
    Node<PublicApiType, RoutesType> registerRouterRoute(final int routeId,
                                                        @NonNull final Class<? extends LifecycleType> lifecycle,
                                                        @NonNull final MapperType mapper,
                                                        @Nullable final TaskManagerFactory taskManagerFactory)
    throws RouteRegistrationNodeException {
        checkCreatingState();
        checkRouteNotRegistered(routeId);
        checkRouteValid(routeId, mapper.getClass());

        final Node<PublicApiType, RoutesType> route = Node.newRouterNode(owner, lifecycle, mapper, taskManagerFactory);
        nodesMap.put(route, routeId);
        routesMap.put(routeId, route);
        return route;
    }

    @NonNull
    @Override
    public
    <LifecycleType extends Lifecycle<SelfApiType, RoutesType>,
    PublicApiType extends Api,
    SelfApiType extends Api,
    RoutesType extends Routes,
    MapperType extends Mapper<LifecycleType, PublicApiType, ?, SelfApiType> & Router<RoutesType>>
    Node<PublicApiType, RoutesType> registerRouterRoute(final int routeId,
                                                        @NonNull final Class<? extends LifecycleType> lifecycle,
                                                        @NonNull final MapperType mapper)
    throws RouteRegistrationNodeException {
        return registerRouterRoute(routeId, lifecycle, mapper, null);
    }

    @NonNull
    @Override
    public Node<?, ?> getRawRoute(final int routeId) {
        return routesMap.get(routeId);
    }

    @Override
    public boolean delete(@NonNull final Node<?, ?> node) {
        final Integer id = nodesMap.remove(node);
        if(id == null) return false;
        routesMap.remove(id);
        return true;
    }

    @NonNull
    @Override
    public Iterator<Node<?, ?>> iterator() {
        return nodesMap.keySet().iterator();
    }

    @Override
    public boolean hasNodes() {
        return nodesMap.size() > 0;
    }

    public void checkAllRoutesRegistered() throws RouteRegistrationNodeException {
        if(routes.getCount() != routesMap.size()) {
            throw new RouteRegistrationNodeException("Some routes not registered in " + owner);
        }
    }

    InternalNodesManagerImpl(@NonNull final Node<?, ?> owner, @NonNull final Routes routes) {
        this.owner = owner;
        this.routes = routes;
        this.routesMap = routes.getCount() == 0? Collections.emptyMap() : new HashMap<>();
    }

    private void checkDestroyState()
    throws DestroyedNodeException {
        if(!owner.getState().isActive()) {
            throw new DestroyedNodeException("Node state not allowing node creation");
        }
    }

    private void checkCreatingState()
    throws RouteRegistrationNodeException {
        if(owner.getState() != State.CREATING) {
            throw new RouteRegistrationNodeException("Node state not allowing route registration");
        }
    }

    private void checkRouteNotRegistered(final int routeId)
    throws RouteRegistrationNodeException {
        if(routesMap.containsKey(routeId)) {
            throw new RouteRegistrationNodeException("Such route already registered");
        }
    }

    private void checkRouteValid(final int routeId, @NonNull final Class<? extends Mapper> mapper)
    throws RouteRegistrationNodeException {
        final Class<? extends Mapper<?, ?, ?, ?>> expectedMapper = routes.getRouteMapper(routeId);
        if(expectedMapper == null) throw new RouteRegistrationNodeException("Such route not exist");
        if(expectedMapper != mapper) throw new RouteRegistrationNodeException("Invalid mapper, expected " + expectedMapper);
    }

    private @NonNull final Node<?, ?> owner;
    private @NonNull final Routes routes;

    private @NonNull final Map<Node<?, ?>, Integer> nodesMap = new HashMap<>();
    private @NonNull final Map<Integer, Node<?, ?>> routesMap;
}