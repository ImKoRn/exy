package com.imkorn.exy.core;

import androidx.annotation.IntDef;

@IntDef({NodeTask.TASK_CREATE,
         NodeTask.TASK_DESTROY,
         NodeTask.TASK_CHILD_DESTROYED,
         NodeTask.TASK_INVOKE,
         NodeTask.TASK_REDIRECT_INVOKE})
@interface NodeTask {
    int TASK_CREATE = 1;
    /**
     * <ul>
     * <li> <strong>arg1</strong> - 1 or 0 == force finishDestroy </li>
     * </ul>
     */
    int TASK_DESTROY = 1 << 1;
    /**
     * <ul>
     * <li> <strong>obj</strong> - destroyed {@link Node} </li>
     * </ul>
     */
    int TASK_CHILD_DESTROYED = 1 << 2;
    /**
     * <ul>
     * <li> <strong>arg1</strong> - method id </li>
     * <li> <strong>obj</strong> - {@link Object} method args </li>
     * </ul>
     */
    int TASK_INVOKE = 1 << 3;
    /**
     * <ul>
     * <li> <strong>arg1</strong> - route id </li>
     * <li> <strong>arg2</strong> - method id in route </li>
     * <li> <strong>obj</strong> - {@link Object} method args</li>
     * </ul>
     */
    int TASK_REDIRECT_INVOKE = 1 << 4;
}
