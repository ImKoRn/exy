package com.imkorn.exy.core.exceptions;

public class ExpectedException extends RuntimeException {
    public ExpectedException() {}

    public ExpectedException(String message) {
        super(message);
    }

    public ExpectedException(String message, Throwable throwable) {
        super(message, throwable);
    }

    public ExpectedException(Throwable throwable) {
        super(throwable);
    }
}
