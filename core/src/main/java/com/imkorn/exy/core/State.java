package com.imkorn.exy.core;

/**
 * {@link Node} lifecycle states
 */
public enum State
{
    IDLE(true),
    CREATING(true),
    CREATED(true),
    DESTROYING(false),
    DESTROYED(false);

    public boolean isActive() {
        return active;
    }

    State(boolean active) {
        this.active = active;
    }

    private final boolean active;
}
