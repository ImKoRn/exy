package com.imkorn.exy.core.exceptions;

/**
 *
 */
public class DestroyedNodeException extends NodeException {
    public DestroyedNodeException(String message) {
        super(message);
    }
}
