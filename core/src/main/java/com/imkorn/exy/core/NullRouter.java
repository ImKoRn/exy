package com.imkorn.exy.core;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.imkorn.exy.mapping.Mapper;
import com.imkorn.exy.mapping.Router;
import com.imkorn.exy.mapping.Routes;
import com.imkorn.exy.mapping.ApiInterceptor;

/**
 * Stub for nodes without {@link Routes}
 */
final class NullRouter implements Router<Routes> {

    public static Router<Routes> get() {
        return INSTANCE;
    }

    @NonNull
    @Override
    public Routes createRoutes(@NonNull ApiInterceptor routeApiInterceptor) {
        return routes;
    }

    @Override
    public String toString() {
        return "No routes";
    }

    private NullRouter() {}

    private final Routes routes = new Routes() {
        @Override
        public int getCount() {
            return 0;
        }
        @Nullable
        @Override
        public Class<? extends Mapper<?, ?, ?, ?>> getRouteMapper(int routeId) {
            return null;
        }
    };
    private static final Router<Routes> INSTANCE = new NullRouter();
}