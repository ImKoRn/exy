package com.imkorn.exy.core.testdata;

import androidx.annotation.NonNull;

import com.imkorn.exy.annotations.DefMappable;
import com.imkorn.exy.annotations.methods.DefAsync;
import com.imkorn.exy.annotations.methods.rx.DefRxMaybe;
import com.imkorn.exy.annotations.methods.rx.DefRxSingle;
import com.imkorn.exy.annotations.methods.rx.DefRxSubject;
import com.imkorn.exy.annotations.methods.rx.RxSubject;
import com.imkorn.exy.annotations.routes.DefRoute;
import com.imkorn.exy.annotations.routes.DefRouter;
import com.imkorn.exy.core.Lifecycle;
import com.imkorn.exy.utils.ThreadHelper;

import io.reactivex.MaybeObserver;
import io.reactivex.Observer;
import io.reactivex.SingleObserver;

@DefMappable(route = true)
@DefRouter({@DefRoute(route = SimpleLifecycleMapper.RouteApi.class)})
public class SimpleLifecycle extends Lifecycle<SimpleLifecycleMapper.SelfApi, SimpleLifecycleMapper.Routes> {

	@Override
	public void onCreate() {
		if(isRouter()) {
			getNodesManager().registerRoute(SimpleLifecycleMapper.Routes.ID_SIMPLE_LIFECYCLE_MAPPER_ROUTE_API,
											SimpleLifecycle.class,
											SimpleLifecycleMapper.get());
		}
	}

	@DefAsync
	public void doTask(@NonNull final ThreadHelper.TaskCallback<Boolean> callback) {
		callback.postResult(true);
	}

	@DefRxSubject(value = Boolean.class, subject = RxSubject.ASYNC)
	public void doRxObservable(Observer<Boolean> observer) {
		observer.onNext(true);
		observer.onComplete();
	}

	@DefRxMaybe(value = Boolean.class)
	public void doRxMaybe(MaybeObserver<Boolean> observer) {
		observer.onSuccess(true);
	}

	@DefRxSingle(value = Boolean.class)
	public void doRxSingle(SingleObserver<Boolean> observer) {
		observer.onSuccess(true);
	}
}
