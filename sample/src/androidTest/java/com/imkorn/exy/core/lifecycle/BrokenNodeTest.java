package com.imkorn.exy.core.lifecycle;

import android.support.test.runner.AndroidJUnit4;
import android.util.Pair;

import com.imkorn.exy.taskmanager.TaskManagerFactory;
import com.imkorn.exy.taskmanager.android.factories.TaskManagerFactoryAndroid;
import com.imkorn.exy.core.Node;
import com.imkorn.exy.core.State;
import com.imkorn.exy.core.exceptions.RouteRegistrationNodeException;
import com.imkorn.exy.core.testdata.BrokenRouterLifecycle;
import com.imkorn.exy.core.testdata.DestroyingRouteRouterLifecycle;
import com.imkorn.exy.core.testdata.RouterLifecycleMapper;
import com.imkorn.exy.utils.NodeAssert;
import com.imkorn.exy.utils.ThreadHelper;

import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(AndroidJUnit4.class)
public class BrokenNodeTest {

	@Test(expected = RouteRegistrationNodeException.class)
	public void routesNotRegistered() throws Throwable {
		ThreadHelper.sync(callback -> {
			final TaskManagerFactory taskManagerFactory = TaskManagerFactoryAndroid.create();
			taskManagerFactory.setTaskManagerInterceptor(taskManager -> {
				taskManager.getThread().setUncaughtExceptionHandler((t, e) -> callback.postError(e));
			});
			Node.newBaseRouterNode(BrokenRouterLifecycle.class, RouterLifecycleMapper.get(), taskManagerFactory);
		});
	}

	@Test
	public void destroyingRoute() throws Throwable {
		final TaskManagerFactory taskManagerFactory = TaskManagerFactoryAndroid.create();
		final Node<RouterLifecycleMapper.Api, RouterLifecycleMapper.Routes> node =
		Node.newBaseRouterNode(DestroyingRouteRouterLifecycle.class, RouterLifecycleMapper.get(), taskManagerFactory);
		NodeAssert.assertUntilState(node, Pair.create(State.DESTROYED, true));
	}
}
