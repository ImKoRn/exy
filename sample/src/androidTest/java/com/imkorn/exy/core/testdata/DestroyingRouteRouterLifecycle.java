package com.imkorn.exy.core.testdata;


public class DestroyingRouteRouterLifecycle extends RouterLifecycle {
	@Override
	public void onCreate() {
		getNodesManager().registerRoute(RouterLifecycleMapper.Routes.ID_EMPTY_LIFECYCLE_MAPPER_ROUTE_API,
										DestroyingEmptyLifecycle.class,
										EmptyLifecycleMapper.get());
	}
}
