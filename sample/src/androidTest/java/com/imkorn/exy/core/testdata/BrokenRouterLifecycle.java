package com.imkorn.exy.core.testdata;


public class BrokenRouterLifecycle extends RouterLifecycle {
	@Override
	public void onCreate() {
		// No registered routes
	}
}
