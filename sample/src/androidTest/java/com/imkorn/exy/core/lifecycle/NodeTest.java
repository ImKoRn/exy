package com.imkorn.exy.core.lifecycle;


import androidx.annotation.NonNull;
import android.util.Pair;

import com.imkorn.exy.taskmanager.TaskManagerFactory;
import com.imkorn.exy.core.Node;
import com.imkorn.exy.core.State;
import com.imkorn.exy.core.testdata.EmptyLifecycle;
import com.imkorn.exy.core.testdata.EmptyLifecycleMapper;
import com.imkorn.exy.core.testdata.RouterLifecycle;
import com.imkorn.exy.core.testdata.RouterLifecycleMapper;
import com.imkorn.exy.utils.ArgsUtils;
import com.imkorn.exy.utils.Identified;
import com.imkorn.exy.utils.NodeAssert;
import com.imkorn.exy.utils.NodeCreator;
import com.imkorn.exy.utils.TaskManagerFactoryCreator;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

@RunWith(Parameterized.class)
public class NodeTest {
	@Parameterized.Parameters(name = "{index}: factoryCreatorId = {0}, nodeCreatorId = {1}")
	@SuppressWarnings("unchecked")
	public static Iterable<? extends Object[]> getArgs() {
		final NodeCreator[] nodeCreators = new NodeCreator[] {
			taskManagerFactory -> Node.newBaseNode(EmptyLifecycle.class, EmptyLifecycleMapper.get(), taskManagerFactory),
			taskManagerFactory -> Node.newBaseRouterNode(RouterLifecycle.class, RouterLifecycleMapper.get(), taskManagerFactory)
		};
		return ArgsUtils.merge(ArgsUtils.getFactoryCreators(), nodeCreators);
	}

	public NodeTest(@NonNull final Identified<TaskManagerFactoryCreator> factoryCreatorIdentified,
					@NonNull final Identified<NodeCreator> nodeCreatorIdentified) {
		this.factoryCreator = factoryCreatorIdentified.get();
		this.nodeCreator = nodeCreatorIdentified.get();
	}

	@Before
	@SuppressWarnings("unchecked")
	public void beforeEach() {
		factory = factoryCreator.create();
		node = nodeCreator.create(factory);
	}

	@After
	public void afterEach() {
		factory.release();
		factory = null;
		node = null;
	}

	@Test(timeout = TIMEOUT)
	public void createNode() throws Throwable {
		NodeAssert.assertState(node, State.CREATED);
	}

	@Test(timeout = TIMEOUT)
	public void destroyForced() throws Throwable {
		// Start destroy
		node.destroy(true);
		// Check destroyed
		NodeAssert.assertUntilState(node, STATES);
	}

	@Test(timeout = TIMEOUT)
	public void destroyAfterCreationNotForced() throws Throwable {
		// Wait full creation
		NodeAssert.assertState(node, State.CREATED);
		// Start destroy
		node.destroy(false);
		// Check destroyed
		NodeAssert.assertUntilState(node, STATES);
	}

	@Test(timeout = TIMEOUT)
	public void destroyAfterCreationForced() throws Throwable {
		// Wait full creation
		NodeAssert.assertState(node, State.CREATED);
		// Start destroy
		node.destroy(true);
		// Check destroyed
		NodeAssert.assertUntilState(node, STATES);
	}

	private TaskManagerFactory factory;
	private Node node;

	private final TaskManagerFactoryCreator factoryCreator;
	private final NodeCreator               nodeCreator;

	private static final long                   TIMEOUT = 2000L; // 2 sec
	@SuppressWarnings("unchecked")
	private static final Pair<State, Boolean>[] STATES  = new Pair[] {
		Pair.create(State.DESTROYED, true),
		Pair.create(State.IDLE, false),
		Pair.create(State.CREATING, false),
		Pair.create(State.CREATED, false)
	};
}
