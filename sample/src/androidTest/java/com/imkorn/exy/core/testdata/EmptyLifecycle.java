package com.imkorn.exy.core.testdata;

import com.imkorn.exy.annotations.DefMappable;
import com.imkorn.exy.core.Lifecycle;
import com.imkorn.exy.mapping.Routes;

@DefMappable(route = true)
public class EmptyLifecycle extends Lifecycle<EmptyLifecycleMapper.SelfApi, Routes> {

}
