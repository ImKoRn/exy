package com.imkorn.exy.core.testdata;

import com.imkorn.exy.annotations.DefMappable;
import com.imkorn.exy.annotations.routes.DefRoute;
import com.imkorn.exy.annotations.routes.DefRouter;
import com.imkorn.exy.core.Lifecycle;

@DefMappable
@DefRouter({@DefRoute(route = EmptyLifecycleMapper.RouteApi.class)})
public class RouterLifecycle extends Lifecycle<RouterLifecycleMapper.SelfApi, RouterLifecycleMapper.Routes> {
	@Override
	public void onCreate() {
		getNodesManager().registerRoute(RouterLifecycleMapper.Routes.ID_EMPTY_LIFECYCLE_MAPPER_ROUTE_API,
										EmptyLifecycle.class,
										EmptyLifecycleMapper.get());
	}
}
