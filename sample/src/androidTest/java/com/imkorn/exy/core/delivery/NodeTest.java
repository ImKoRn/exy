package com.imkorn.exy.core.delivery;

import androidx.annotation.NonNull;

import com.imkorn.exy.taskmanager.TaskManagerFactory;
import com.imkorn.exy.core.Node;
import com.imkorn.exy.core.testdata.SimpleLifecycle;
import com.imkorn.exy.core.testdata.SimpleLifecycleMapper;
import com.imkorn.exy.utils.Identified;
import com.imkorn.exy.utils.NodeCreator;
import com.imkorn.exy.utils.ArgsUtils;
import com.imkorn.exy.utils.TaskManagerFactoryCreator;
import com.imkorn.exy.utils.ThreadHelper;

import junit.framework.Assert;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import io.reactivex.MaybeObserver;
import io.reactivex.Observer;
import io.reactivex.SingleObserver;
import io.reactivex.disposables.Disposable;

@RunWith(Parameterized.class)
public class NodeTest {
	@Parameterized.Parameters(name = "{index}: factoryCreatorId = {0}, nodeCreatorId = {1}")
	@SuppressWarnings("unchecked")
	public static Iterable<? extends Object[]> getArgs() {
		final NodeCreator<SimpleLifecycleMapper.Api, ?>[] nodeCreators = new NodeCreator[] {
			taskManagerFactory -> Node.newBaseNode(SimpleLifecycle.class, SimpleLifecycleMapper.get(), taskManagerFactory),
			taskManagerFactory -> Node.newBaseRouterNode(SimpleLifecycle.class, SimpleLifecycleMapper.get(), taskManagerFactory)
		};
		return ArgsUtils.merge(ArgsUtils.getFactoryCreators(), nodeCreators);
	}

	public NodeTest(@NonNull final Identified<TaskManagerFactoryCreator> taskManagerFactoryCreatorIdentified,
					@NonNull final Identified<NodeCreator<SimpleLifecycleMapper.Api, ?>> nodeCreatorIdentified) {
		this.factoryCreator = taskManagerFactoryCreatorIdentified.get();
		this.nodeCreator = nodeCreatorIdentified.get();
	}

	@Before
	public void beforeEach() {
		taskManagerFactory = factoryCreator.create();
		node = nodeCreator.create(taskManagerFactory);
	}

	@After
	public void afterEach() {
		taskManagerFactory.release();
		taskManagerFactory = null;
		node = null;
	}

	@Test
	public void async() throws Throwable {
		Assert.assertTrue(ThreadHelper.sync(callback -> node.getApi().doTaskAsync(callback)));
	}

	@Test
	public void rxSubject() throws Throwable {
		Assert.assertTrue(ThreadHelper.sync(callback -> node.getApi().doRxObservableAsObservable().subscribe(new Observer<Boolean>() {
			@Override
			public void onComplete() {
				callback.postResult(true);
			}

			@Override
			public void onSubscribe(final Disposable d) {}

			@Override
			public void onNext(final Boolean b) {}

			@Override
			public void onError(final Throwable e) {}
		})));
	}

	@Test
	public void rxSingle() throws Throwable {
		Assert.assertTrue(ThreadHelper.sync(callback -> node.getApi().doRxSingleAsSingle().subscribe(new SingleObserver<Boolean>() {
			@Override
			public void onSuccess(final Boolean b) {
				callback.postResult(b);
			}

			@Override
			public void onSubscribe(final Disposable d) {}

			@Override
			public void onError(final Throwable e) {}
		})));
	}

	@Test
	public void rxMaybe() throws Throwable {
		Assert.assertTrue(ThreadHelper.sync(callback -> node.getApi().doRxMaybeAsMaybe().subscribe(new MaybeObserver<Boolean>() {
			@Override
			public void onComplete() {
				callback.postResult(true);
			}

			@Override
			public void onSubscribe(final Disposable d) {}

			@Override
			public void onSuccess(final Boolean b) {
				callback.postResult(b);
			}

			@Override
			public void onError(final Throwable e) {}
		})));
	}

	private TaskManagerFactory taskManagerFactory;
	private Node<SimpleLifecycleMapper.Api, ?> node;

	private final TaskManagerFactoryCreator factoryCreator;
	private final NodeCreator<SimpleLifecycleMapper.Api, ?> nodeCreator;
}
