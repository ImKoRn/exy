package com.imkorn.exy.core.testdata;

public class DestroyingEmptyLifecycle extends EmptyLifecycle {
	@Override
	public void onCreate() {
		destroy(false);
	}
}
