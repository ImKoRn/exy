package com.imkorn.exy.utils;

import android.os.SystemClock;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.concurrent.TimeoutException;

public class ThreadHelper {
	public interface Worker<ResultType> {
		void runTask(@NonNull TaskCallback<ResultType> callback) throws Throwable;
	}

	public interface TaskCallback<ResultType> {
		void postResult(ResultType result);
		void postError(Throwable error);
	}

	public static <ResultType> ResultType sync(@NonNull final Worker<ResultType> worker) throws Throwable {

		final DefaultTaskCallback<ResultType> taskCallback = new DefaultTaskCallback<>();

		worker.runTask(taskCallback);

		for(;;) {
			synchronized(taskCallback) {
				if(taskCallback.finished)
				{
					final Throwable throwable = taskCallback.throwable;
					if(throwable != null) throw throwable;

					return taskCallback.result;
				}

				taskCallback.wait();
			}
		}
	}

	public static <ResultType> ResultType sync(@NonNull final Worker<ResultType> worker, final long timeout) throws Throwable {

		final DefaultTaskCallback<ResultType> taskCallback = new DefaultTaskCallback<>();

		worker.runTask(taskCallback);
		final long start = SystemClock.elapsedRealtime();
		long now = start;
		for(;;) {

			synchronized(taskCallback) {
				if(taskCallback.finished)
				{
					final Throwable throwable = taskCallback.throwable;
					if(throwable != null) throw throwable;

					return taskCallback.result;
				}

				final long spent = now - start;
				final long left = timeout - spent;

				if(left <= 0) throw new TimeoutException();

				taskCallback.wait(left);
				now = SystemClock.elapsedRealtime();
			}
		}
	}

	private static class DefaultTaskCallback<ResultType> implements TaskCallback<ResultType> {

		@Override
		public synchronized void postResult(@Nullable final ResultType result) {
			if(finished) return;
			finished = true;

			this.result = result;
			notifyAll();
		}

		@Override
		public synchronized void postError(@NonNull final Throwable throwable) {
			if(finished) return;
			finished = true;

			if(throwable == null) throw new NullPointerException("Exception can't be null");

			this.throwable = throwable;
			notifyAll();
		}

		private boolean finished = false;
		private Throwable throwable = null;
		private ResultType result = null;
	}

	private ThreadHelper() {
		//no instance
	}
}
