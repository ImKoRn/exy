package com.imkorn.exy.utils;

import android.util.Pair;

import com.imkorn.exy.core.Node;
import com.imkorn.exy.core.State;

import junit.framework.Assert;

public class NodeAssert {
	public static void assertState(final Node<?, ?> node, final State state) throws Throwable {
		Assert.assertTrue(ThreadHelper.sync(callback -> node.post(() -> callback.postResult(node.getState() == state))));
	}

	@SafeVarargs
	public static void assertUntilState(final Node<?, ?> node, Pair<State, Boolean>... states) throws Throwable
	{
		Assert.assertTrue(ThreadHelper.sync(callback -> {
			final Runnable destroyCheck = new Runnable()
			{
				@Override
				public void run()
				{
					final State state = node.getState();
					for(final Pair<State, Boolean> pairs : states)
					{
						if(pairs.first == state) {
							callback.postResult(pairs.second);
							return;
						}
					}

					node.post(this);
				}
			};

			node.post(destroyCheck);
		}));
	}

	private NodeAssert() {
		//no instance
	}
}
