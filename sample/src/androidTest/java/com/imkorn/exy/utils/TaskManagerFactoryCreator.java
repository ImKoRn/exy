package com.imkorn.exy.utils;

import androidx.annotation.NonNull;

import com.imkorn.exy.taskmanager.TaskManagerFactory;

public interface TaskManagerFactoryCreator {
	@NonNull
	TaskManagerFactory create();
}
