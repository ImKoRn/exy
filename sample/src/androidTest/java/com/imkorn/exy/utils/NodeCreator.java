package com.imkorn.exy.utils;

import androidx.annotation.NonNull;

import com.imkorn.exy.taskmanager.TaskManagerFactory;
import com.imkorn.exy.core.Node;
import com.imkorn.exy.mapping.Api;
import com.imkorn.exy.mapping.Routes;

public interface NodeCreator<ApiType extends Api, RoutesType extends Routes> {
	@NonNull
	Node<ApiType, RoutesType> create(@NonNull final TaskManagerFactory taskManagerFactory);
}
