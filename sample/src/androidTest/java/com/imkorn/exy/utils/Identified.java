package com.imkorn.exy.utils;

import androidx.annotation.NonNull;

public final class Identified<Type> {

	public Identified(final int id, @NonNull final Type value) {
		this.id = id;
		this.value = value;
	}

	@Override
	public String toString() {
		return String.valueOf(id);
	}

	public Type get() {
		return value;
	}

	private final int id;
	private final Type value;
}
