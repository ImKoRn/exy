package com.imkorn.exy.utils;

import android.os.HandlerThread;
import androidx.annotation.NonNull;

import com.imkorn.exy.taskmanager.android.factories.TaskManagerFactoryAndroid;
import com.imkorn.exy.taskmanager.android.factories.TaskManagerFactoryAndroidCached;
import com.imkorn.exy.taskmanager.android.factories.TaskManagerFactoryAndroidDynamic;
import com.imkorn.exy.taskmanager.android.factories.TaskManagerFactoryAndroidTarget;

import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;

public class ArgsUtils
{

	@NonNull
	public static TaskManagerFactoryCreator[] getFactoryCreators() {
		return new TaskManagerFactoryCreator[] {
			TaskManagerFactoryAndroid::create,
			TaskManagerFactoryAndroidCached::create,
			TaskManagerFactoryAndroidDynamic::create,
			() -> {
				final HandlerThread handlerThread = new HandlerThread("TaskManagerFactoryAndroidTarget");
				handlerThread.start();
				return TaskManagerFactoryAndroidTarget.create(handlerThread.getLooper());
			}
		};
	}

	public static Iterable<? extends Object[]> merge(final Object[]... args) {
		return merge(true, args);
	}

	public static Iterable<? extends Object[]> merge(final boolean identified, final Object[]... args) {
		if(args.length == 0) return Collections.emptyList();

		final Collection<Object[]> merged = new LinkedList<>();
		merge(args, merged, identified, new Object[args.length], 0);
		return merged;
	}

	public static void merge(@NonNull final Object[][] source,
							 @NonNull final Collection<Object[]> target,
							 final boolean identified,
							 @NonNull final Object[] arg,
							 final int position) {
		final boolean tail = source.length == position + 1;
		final Object[] args = source[position];
		for(int index = 0; index < args.length; index++) {
			arg[position] = identified? new Identified<>(index, args[index]) : args[index];
			if(tail) {
				target.add(arg.clone());
			} else {
				merge(source, target, identified, arg, position + 1);
			}
		}
	}

	private ArgsUtils() {
		//no instance
	}
}
