package com.imkorn.exy.taskmanager;

import androidx.annotation.NonNull;

import com.imkorn.exy.utils.ArgsUtils;
import com.imkorn.exy.utils.TaskManagerFactoryCreator;

import junit.framework.Assert;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Random;

@RunWith(Parameterized.class)
public class TaskManagerFactoryTest {
	@Parameterized.Parameters
	public static Object[] getArgs() {
		return ArgsUtils.getFactoryCreators();
	}

	public TaskManagerFactoryTest(@NonNull final TaskManagerFactoryCreator factoryCreator) {
		this.factoryCreator = factoryCreator;
	}

	@Before
	public void beforeEach() {
		taskManagerFactory = factoryCreator.create();
	}

	@After
	public void afterEach() {
		taskManagerFactory.release();
		taskManagerFactory = null;
	}

	@Test
	public void taskManagerCreate() {
		final int count = getTaskManagerCreateCount();

		for(int index = 0; index < count; index++) {
			final String failMsg = index + " of " + count;
			final TaskManager taskManager = taskManagerFactory.getTaskManager();
			Assert.assertNotNull(failMsg, taskManager);
			Assert.assertNotNull(failMsg, taskManager.getThread());
			Assert.assertTrue(failMsg, taskManager.getThread().isAlive());
		}
	}

	@Test(expected = IllegalStateException.class)
	public void factoryRelease() {
		taskManagerFactory.release();
		Assert.assertTrue(taskManagerFactory.isReleased());
		taskManagerFactory.getTaskManager();
	}

	private static int getTaskManagerCreateCount() {
		final Random random = new Random();
		return random.nextInt(4);
	}

	private TaskManagerFactory taskManagerFactory;
	private final TaskManagerFactoryCreator factoryCreator;
}
