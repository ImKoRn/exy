package com.imkorn.exy.taskmanager.android;

import android.os.Process;
import android.support.test.runner.AndroidJUnit4;

import com.imkorn.exy.taskmanager.TaskManager;
import com.imkorn.exy.taskmanager.TaskManagerFactory;
import com.imkorn.exy.taskmanager.android.factories.TaskManagerFactoryAndroidCached;
import com.imkorn.exy.taskmanager.android.factories.TaskManagerFactoryAndroidDynamic;


import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(AndroidJUnit4.class)
public class TaskManagerFactoryTest {

    @Test
    public void cachedFactory() {
        TaskManagerFactory factoryAndroidCached = TaskManagerFactoryAndroidCached.create(TASK_MANAGERS_COUNT, Process.THREAD_PRIORITY_DEFAULT);

        final long[] threadIds = new long[TASK_MANAGERS_COUNT];
        for(int index = 0; index < TASK_MANAGERS_COUNT; index++) {
            final TaskManager taskManager = factoryAndroidCached.getTaskManager();
            Assert.assertNotNull(taskManager);
            Assert.assertNotNull(taskManager.getThread());
            threadIds[index] = taskManager.getThread().getId();
        }

        for(int index = 0; index < TASK_MANAGERS_COUNT; index++) {
            final TaskManager taskManager = factoryAndroidCached.getTaskManager();
            Assert.assertNotNull(taskManager);
            Assert.assertNotNull(taskManager.getThread());
            Assert.assertEquals(threadIds[index], taskManager.getThread().getId());
        }

    }

    @Test
    public void dynamicFactory() {
        TaskManagerFactory factoryAndroidCached = TaskManagerFactoryAndroidDynamic.create();

        final long[] threadIds = new long[TASK_MANAGERS_COUNT];
        for(int index = 0; index < TASK_MANAGERS_COUNT; index++) {
            final TaskManager taskManager = factoryAndroidCached.getTaskManager();
            Assert.assertNotNull(taskManager);
            Assert.assertNotNull(taskManager.getThread());
            threadIds[index] = taskManager.getThread().getId();
        }

        for(int index = 0; index < TASK_MANAGERS_COUNT; index++) {
            final TaskManager taskManager = factoryAndroidCached.getTaskManager();
            Assert.assertNotNull(taskManager);
            Assert.assertNotNull(taskManager.getThread());
            final long threadId = taskManager.getThread().getId();
            for(long previousThreadId : threadIds) {
                Assert.assertNotEquals(previousThreadId, threadId);
            }
        }
    }

    private static final int TASK_MANAGERS_COUNT = 10;
}
