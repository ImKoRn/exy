package com.imkorn.exy.taskmanager;

import android.os.HandlerThread;
import androidx.annotation.NonNull;

import com.imkorn.exy.taskmanager.android.TaskManagerAndroid;
import com.imkorn.exy.utils.ThreadHelper;

import junit.framework.Assert;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Random;
import java.util.concurrent.TimeoutException;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(Parameterized.class)
public class TaskManagerTest {

    @Parameterized.Parameters
    public static Object[] getArgs() {
        return new TaskManagerCreator[] {
            new TaskManagerCreatorAndroid()
        };
    }

    public TaskManagerTest(@NonNull final TaskManagerCreator taskManagerCreator) {
        this.taskManagerCreator = taskManagerCreator;
    }

    @Before
    public void beforeEach() {
        taskManager = taskManagerCreator.create();
    }

    @After
    public void afterEach() throws Throwable {
        taskManagerCreator.release();
        taskManager = null;
    }

    @Test
    public void creation() {
        Assert.assertNotNull(taskManager);
        Assert.assertNotNull(taskManager.getThread());
        Assert.assertTrue(taskManager.getThread().isAlive());
    }

    @Test
    public void postRunnable() throws Throwable {
        Assert.assertTrue(ThreadHelper.sync(callback -> taskManager.postRunnable(() -> callback.postResult(true))));
    }

    @Test
    public void postDelayedRunnable() throws Throwable {
        Assert.assertTrue(ThreadHelper.sync(callback -> {
            final long msStart = System.currentTimeMillis();
            taskManager.postRunnable(() -> {
                final long msEnd = System.currentTimeMillis();
                final long spend = msEnd - msStart;
                callback.postResult(spend >= DELAYED_RUNNABLE_POST_DELAY);
            }, DELAYED_RUNNABLE_POST_DELAY);
        }));
    }

    @Test(expected = TimeoutException.class)
    public void removeRunnable() throws Throwable {
        Assert.assertTrue(ThreadHelper.sync(callback -> {
            final Runnable runnable = () -> callback.postResult(false);
            taskManager.postRunnable(runnable, DELAYED_RUNNABLE_POST_DELAY);
            taskManager.removeRunnable(runnable);
        }, DELAYED_RUNNABLE_POST_DELAY + DELIVERING_TIME_DELAY));
    }

    @Test
    public void postType() throws Throwable {
        final Random random = new Random();

        final int type = random.nextInt();
        final long threadId = taskManager.getThread().getId();

        Assert.assertTrue(ThreadHelper.sync(callback -> {
            taskManager.setTaskHandler((rtype, rarg1, rarg2, rargObj) -> {
                callback.postResult(type == rtype &&
                                    threadId == Thread.currentThread().getId());
            });
            taskManager.postTask(type);
        }));
    }

    @Test
    public void postTypeArgs() throws Throwable {
        final Random random = new Random();

        final int type = random.nextInt();
        final int arg1 = random.nextInt();
        final int arg2 = random.nextInt();
        final long threadId = taskManager.getThread().getId();

        Assert.assertTrue(ThreadHelper.sync(callback -> {
            taskManager.setTaskHandler((rtype, rarg1, rarg2, rargObj) -> {
                callback.postResult(type == rtype &&
                                    arg1 == rarg1 &&
                                    arg2 == rarg2 &&
                                    threadId == Thread.currentThread().getId());
            });
            taskManager.postTask(type, arg1, arg2);
        }));
    }

    @Test
    public void postTypeArgsObj() throws Throwable {
        final Random random = new Random();

        final int type = random.nextInt();
        final int arg1 = random.nextInt();
        final int arg2 = random.nextInt();
        final Object obj = new Object();
        final long threadId = taskManager.getThread().getId();

        Assert.assertTrue(ThreadHelper.sync(callback -> {
            taskManager.setTaskHandler((rtype, rarg1, rarg2, rargObj) -> {
                callback.postResult(type == rtype &&
                                    arg1 == rarg1 &&
                                    arg2 == rarg2 &&
                                    obj.equals(rargObj) &&
                                    threadId == Thread.currentThread().getId());
			});
            taskManager.postTask(type, arg1, arg2, obj);
        }));
    }

    @Test
    public void postTypeObj() throws Throwable {
        final Random random = new Random();

        final int type = random.nextInt();
        final Object obj = new Object();
        final long threadId = taskManager.getThread().getId();

        Assert.assertTrue(ThreadHelper.sync(callback -> {
            taskManager.setTaskHandler((rtype, rarg1, rarg2, rargObj) -> {
                callback.postResult(type == rtype &&
                                    obj.equals(rargObj) &&
                                    threadId == Thread.currentThread().getId());
            });
            taskManager.postTask(type, obj);
        }));
    }

    private TaskManager taskManager;
    private final TaskManagerCreator taskManagerCreator;

    private interface TaskManagerCreator {
        TaskManager create();
        void release() throws Throwable;
    }

    private static class TaskManagerCreatorAndroid implements TaskManagerCreator {

        @Override
        public TaskManager create() {
            handlerThread = new HandlerThread("Test");
            handlerThread.start();
            return new TaskManagerAndroid(handlerThread.getLooper());
        }

        @Override
        public void release() throws Throwable {
            handlerThread.quit();
            handlerThread.join();
            handlerThread = null;
        }

        private HandlerThread handlerThread;
    }

    private static final long DELIVERING_TIME_DELAY = 50;
    private static final long DELAYED_RUNNABLE_POST_DELAY = 2000;
}