package com.imkorn.exy.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Defines mappable
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.SOURCE)
public @interface DefMappable {
    /**
     * Custom name for generated classes, by default it's target class name
     * <p><strong> All generated classes have own endings </strong></p>
     */
    String name() default "";
    /**
     * Determinate need compiler map parent <strong> not overridden </strong> methods, or not
     */
    boolean deepMapping() default false;

    /**
     * Make this mappable object route and generate additional class
     */
    boolean route() default false;
}
