package com.imkorn.exy.annotations.methods.rx;

import com.imkorn.exy.annotations.ApiAccess;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Defines Rx Subject as response engine
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.SOURCE)
public @interface DefRxSubject {
    ApiAccess[] access() default {ApiAccess.PUBLIC, ApiAccess.ROUTE};

    Class<?> value();

    RxSubject subject();
}
