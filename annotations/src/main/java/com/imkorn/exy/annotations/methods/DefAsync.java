package com.imkorn.exy.annotations.methods;

import com.imkorn.exy.annotations.ApiAccess;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Defines async method that not return anything
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.SOURCE)
public @interface DefAsync {
    ApiAccess[] access() default {ApiAccess.PUBLIC, ApiAccess.ROUTE};
}
