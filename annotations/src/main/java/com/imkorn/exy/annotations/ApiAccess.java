package com.imkorn.exy.annotations;

/**
 * Describes access scopes
 */
public enum ApiAccess {
    PUBLIC, CHILD, SELF, ROUTE
}
