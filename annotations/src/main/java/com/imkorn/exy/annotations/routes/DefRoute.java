package com.imkorn.exy.annotations.routes;

import com.imkorn.exy.mapping.RouteApi;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Defines route in router
 */
@Retention(RetentionPolicy.SOURCE)
public @interface DefRoute {
    /**
     * @return Must always return full path to class.
     * <p>e.g. com.example.package.MapperClass.RouteApi</p>
     */
    Class<? extends RouteApi> route();
    String name() default "";
}
