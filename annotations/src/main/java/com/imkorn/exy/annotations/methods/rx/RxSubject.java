package com.imkorn.exy.annotations.methods.rx;

/**
 * Defines subject types in RxJava Subject
 */
public enum RxSubject {
    ASYNC, REPLAY, PUBLISH, BEHAVIOUR, UNICAST
}
