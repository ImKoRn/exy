package com.imkorn.exy.taskmanager.android;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.imkorn.exy.taskmanager.TaskHandler;
import com.imkorn.exy.taskmanager.TaskManager;

public class TaskManagerAndroid extends Handler implements TaskManager {

    public TaskManagerAndroid(@NonNull final Looper looper) {
        super(looper);
    }

    @Override
    public void handleMessage(@NonNull final Message msg) {
        final TaskHandler taskHandler = this.taskHandler;

        if(taskHandler != null) {
            taskHandler.handle(msg.what, msg.arg1, msg.arg2, msg.obj);
        }
    }

    @Override
    public void postRunnable(@NonNull final Runnable runnable) {
        post(runnable);
    }

    @Override
    public void postRunnable(@NonNull final Runnable runnable, final long delayMillis) {
        postDelayed(runnable, delayMillis);
    }

    @Override
    public void removeRunnable(@Nullable final Runnable runnable) {
        removeCallbacks(runnable);
    }

    @Override
    public void postTask(final int type, final int arg1, final int arg2, @Nullable final Object argObj) {
        sendMessage(obtainMessage(type, arg1, arg2, argObj));
    }

    @Override
    public void postTask(final int type, final int arg1, final int arg2) {
        sendMessage(obtainMessage(type, arg1, arg2));
    }

    @Override
    public void postTask(final int type, @Nullable final Object argObj) {
        sendMessage(obtainMessage(type, argObj));
    }

    @Override
    public void postTask(final int type) {
        sendEmptyMessage(type);
    }

    @Override
    public void postTaskAtFront(final int type, final int arg1, final int arg2, @Nullable final Object argObj) {
        sendMessageAtFrontOfQueue(obtainMessage(type, arg1, arg2, argObj));
    }

    @Override
    public void postTaskAtFront(final int type, final int arg1, final int arg2) {
        sendMessageAtFrontOfQueue(obtainMessage(type, arg1, arg2));
    }

    @Override
    public void postTaskAtFront(final int type, @Nullable final Object argObj) {
        sendMessageAtFrontOfQueue(obtainMessage(type, argObj));
    }

    @Override
    public void postTaskAtFront(final int type) {
        sendMessageAtFrontOfQueue(obtainMessage(type));
    }

    @Override
    @NonNull
    public Thread getThread() {
        return getLooper().getThread();
    }

    @Override
    public void setTaskHandler(@Nullable final TaskHandler taskHandler) {
        this.taskHandler = taskHandler;
    }

    @Override
    public void clearAll() {
        removeCallbacksAndMessages(null);
    }

    @Nullable
    private volatile TaskHandler taskHandler;
}
