package com.imkorn.exy.taskmanager.android;

import android.os.Process;
import androidx.annotation.IntDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * {@link Process} priorities
 */
@Retention(RetentionPolicy.SOURCE)
@IntDef({Process.THREAD_PRIORITY_AUDIO,
         Process.THREAD_PRIORITY_BACKGROUND,
         Process.THREAD_PRIORITY_DEFAULT,
         Process.THREAD_PRIORITY_DISPLAY,
         Process.THREAD_PRIORITY_FOREGROUND,
         Process.THREAD_PRIORITY_LESS_FAVORABLE,
         Process.THREAD_PRIORITY_MORE_FAVORABLE,
         Process.THREAD_PRIORITY_URGENT_DISPLAY,
         Process.THREAD_PRIORITY_URGENT_AUDIO,
         Process.THREAD_PRIORITY_LOWEST})
public @interface ThreadPriority {
}