package com.imkorn.exy.taskmanager.android.factories;

import android.os.HandlerThread;
import android.os.Process;
import androidx.annotation.NonNull;

import com.imkorn.exy.taskmanager.TaskManager;
import com.imkorn.exy.taskmanager.TaskManagerFactory;
import com.imkorn.exy.taskmanager.android.TaskManagerAndroid;
import com.imkorn.exy.taskmanager.android.ThreadPriority;

import java.util.concurrent.atomic.AtomicInteger;

public final class TaskManagerFactoryAndroid extends TaskManagerFactory {

    @NonNull
    public static TaskManagerFactory create() {
        return create(generateName(Process.THREAD_PRIORITY_DEFAULT), Process.THREAD_PRIORITY_DEFAULT);
    }
    @NonNull
    public static TaskManagerFactory create(@NonNull final String name) {
        return create(name, Process.THREAD_PRIORITY_DEFAULT);
    }
    @NonNull
    public static TaskManagerFactory create(@ThreadPriority final int threadPriority) {
        return create(generateName(threadPriority), threadPriority);
    }
    @NonNull
    public static TaskManagerFactory create(@NonNull final String name, @ThreadPriority final int threadPriority) {
        return new TaskManagerFactoryAndroid(name, threadPriority);
    }

    @Override
    protected TaskManager getTaskManagerInternal() {
        return new TaskManagerAndroid(handlerThread.getLooper());
    }

    @Override
    protected void releaseInternal() {
        handlerThread.quit();
    }

    private TaskManagerFactoryAndroid(@NonNull final String name, @ThreadPriority final int threadPriority) {
        handlerThread = new HandlerThread(name, threadPriority);
        handlerThread.start();
    }

    @NonNull
    private static String generateName(@ThreadPriority int threadPriority) {
        return PREFIX + '-' + INDEX.getAndIncrement() + ':' + threadPriority;
    }

    private final HandlerThread handlerThread;

    private static final String PREFIX = "ExySingle";
    private static final AtomicInteger INDEX = new AtomicInteger(0);
}
