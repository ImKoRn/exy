package com.imkorn.exy.taskmanager.android.factories;

import android.os.Looper;
import androidx.annotation.NonNull;

import com.imkorn.exy.taskmanager.TaskManager;
import com.imkorn.exy.taskmanager.TaskManagerFactory;
import com.imkorn.exy.taskmanager.android.TaskManagerAndroid;

public final class TaskManagerFactoryAndroidTarget extends TaskManagerFactory {
    @NonNull
    public static TaskManagerFactory create(@NonNull final Looper looper) {
        if(looper == null) throw new IllegalArgumentException("Looper must not be null");
        return new TaskManagerFactoryAndroidTarget(looper);
    }

    @NonNull
    @Override
    protected TaskManager getTaskManagerInternal() {
        return new TaskManagerAndroid(looper);
    }

    @Override
    protected void releaseInternal() {
        looper.quit();
    }

    private TaskManagerFactoryAndroidTarget(@NonNull final Looper looper) {
        this.looper = looper;
    }

    private final Looper looper;
}
