package com.imkorn.exy.taskmanager.android.factories;

import android.os.Process;
import androidx.annotation.NonNull;

import com.imkorn.exy.taskmanager.TaskManager;
import com.imkorn.exy.taskmanager.TaskManagerFactory;
import com.imkorn.exy.taskmanager.android.ThreadPriority;

import java.util.concurrent.atomic.AtomicInteger;

public final class TaskManagerFactoryAndroidCached extends TaskManagerFactory {

    @NonNull
    public static TaskManagerFactory create() {
        return create(Process.THREAD_PRIORITY_DEFAULT);
    }
    @NonNull
    public static TaskManagerFactory create(@ThreadPriority final int threadPriority) {
        final int cpuCount = Runtime.getRuntime().availableProcessors();
        return create(Math.max(2, Math.min(cpuCount - 1, 4)), threadPriority);
    }
    @NonNull
    public static TaskManagerFactory create(final int capacity, @ThreadPriority final int threadPriority) {
        if(capacity < MIN_CAPACITY) {
            throw new IllegalArgumentException(ERROR_WRONG_CAPACITY);
        }

        return new TaskManagerFactoryAndroidCached(capacity, threadPriority);
    }

    @NonNull
    @Override
    protected TaskManager getTaskManagerInternal() {
        final int original = pointer.get();
        // Balanced pointer increment
        pointer.compareAndSet(original, (original + 1) % cache.length);

        final int index = original % cache.length;

        if(cache[index] != null) {
            return cache[index].getTaskManager();
        }

        synchronized(monitors[index]) {
            if(cache[index] != null) {
                return cache[index].getTaskManager();
            }

            cache[index] = TaskManagerFactoryAndroid.create(generateName(threadPriority, index),
                                                            threadPriority);
            return cache[index].getTaskManager();
        }
    }

    @Override
    protected void releaseInternal() {
        for(int index = 0; index < cache.length; index++) {
            final TaskManagerFactory taskManagerFactory = cache[index];

            if(taskManagerFactory != null) {
                taskManagerFactory.release();
                cache[index] = null;
            }
        }
    }

    private TaskManagerFactoryAndroidCached(final int capacity, final int threadPriority) {
        this.cache = new TaskManagerFactory[capacity];
        this.monitors = new Object[capacity];
        this.threadPriority = threadPriority;

        for(int index = 0; index < this.monitors.length; index++) {
            this.monitors[index] = new Object();
        }
    }


    @NonNull
    private String generateName(@ThreadPriority final int threadPriority, final int index) {
        return PREFIX + ':' + instanceId + '-' + index + ':' + threadPriority;
    }

    private final int threadPriority;
    private final TaskManagerFactory[] cache;
    private final Object[] monitors;
    private final int instanceId = INSTANCE_ID.getAndIncrement();
    private final AtomicInteger pointer = new AtomicInteger(0);

    private static final String PREFIX = "ExyCached";
    private static final int MIN_CAPACITY = 1;
    private static final AtomicInteger INSTANCE_ID = new AtomicInteger(0);
    private static final String ERROR_WRONG_CAPACITY = "Capacity must be more or equal than - " + MIN_CAPACITY;
}
