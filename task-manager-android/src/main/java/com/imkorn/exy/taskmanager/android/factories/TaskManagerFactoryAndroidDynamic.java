package com.imkorn.exy.taskmanager.android.factories;

import android.os.Process;
import androidx.annotation.NonNull;

import com.imkorn.exy.taskmanager.TaskManager;
import com.imkorn.exy.taskmanager.TaskManagerFactory;
import com.imkorn.exy.taskmanager.android.ThreadPriority;

import java.util.Collection;
import java.util.LinkedList;
import java.util.concurrent.atomic.AtomicInteger;

public final class TaskManagerFactoryAndroidDynamic extends TaskManagerFactory {

    @NonNull
    public static TaskManagerFactory create() {
        return create(Process.THREAD_PRIORITY_DEFAULT);
    }
    @NonNull
    public static TaskManagerFactory create(@ThreadPriority final int threadPriority) {
        return new TaskManagerFactoryAndroidDynamic(threadPriority);
    }

    @NonNull
    @Override
    protected TaskManager getTaskManagerInternal() {
        final TaskManagerFactory taskManagerFactory = TaskManagerFactoryAndroid.create(generateName(threadPriority), threadPriority);

        synchronized(factoriesCache) {
            factoriesCache.add(taskManagerFactory);
        }

        return taskManagerFactory.getTaskManager();
    }

    @Override
    protected void releaseInternal() {
        for(TaskManagerFactory factory : factoriesCache) factory.release();
        factoriesCache.clear();
    }

    private TaskManagerFactoryAndroidDynamic(@ThreadPriority final int threadPriority) {
        this.threadPriority = threadPriority;
    }

    @NonNull
    private static String generateName(@ThreadPriority final int threadPriority) {
        return PREFIX + '-' + INDEX.getAndIncrement() + ':' + threadPriority;
    }

    @ThreadPriority
    private final int threadPriority;
    private final Collection<TaskManagerFactory> factoriesCache = new LinkedList<>();

    private static final String PREFIX = "ExyDynamic";
    private static final AtomicInteger INDEX = new AtomicInteger(0);
}
