package com.imkorn.exy.mapping;

import androidx.annotation.NonNull;

public abstract class RouteApi extends Api {
    protected RouteApi(final int id, @NonNull final ApiInterceptor interceptor) {
        super(id, interceptor);
    }
}
