package com.imkorn.exy.mapping;

import androidx.annotation.AnyThread;
import androidx.annotation.Nullable;

@AnyThread
public interface Routes {
    int getCount();
    @Nullable
    Class<? extends Mapper<?, ?, ?, ?>> getRouteMapper(int routeId);
}
