package com.imkorn.exy.mapping;

import androidx.annotation.AnyThread;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

/**
 * Describes how will be mapped non direct call for real method
 */
@AnyThread
public interface Mapper<MappableType, PublicApiType extends Api, ChildApiType extends Api, SelfApiType extends Api> {

    @NonNull
    PublicApiType createPublicApi(int id, @NonNull ApiInterceptor apiInterceptor);

    @NonNull
    ChildApiType createChildApi(int id, @NonNull ApiInterceptor apiInterceptor);

    @NonNull
    SelfApiType createSelfApi(int id, @NonNull ApiInterceptor apiInterceptor);

    void map(@NonNull MappableType mappable, int methodId, @Nullable Object args) throws Throwable;
}
