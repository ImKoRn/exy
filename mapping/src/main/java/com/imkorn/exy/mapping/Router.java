package com.imkorn.exy.mapping;

import androidx.annotation.AnyThread;
import androidx.annotation.NonNull;

@AnyThread
public interface Router<RoutesType extends Routes> {
    @NonNull
    RoutesType createRoutes(@NonNull ApiInterceptor routeApiInterceptor);
}
