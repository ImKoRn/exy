package com.imkorn.exy.mapping;

import androidx.annotation.AnyThread;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

/**
 * Base api for delegate method invoking
 */
@AnyThread
public abstract class Api {

    public abstract Class<? extends Mapper<?, ?, ?, ?>> getMapper();

    protected Api(final int id, @NonNull ApiInterceptor interceptor) {
        this.id = id;
        this.interceptor = interceptor;
    }
    /**
     * Make call to mappable type through {@link ApiInterceptor}
     *
     * @param methodId  id of callable method
     * @param args      arguments for method, may be null
     */
    protected final void invoke(int methodId, @Nullable Object args) {
        interceptor.invoke(id, methodId, args);
    }

    private final int id;
    private final ApiInterceptor interceptor;
}
