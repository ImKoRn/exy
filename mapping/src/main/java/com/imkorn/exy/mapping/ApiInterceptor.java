package com.imkorn.exy.mapping;

import androidx.annotation.AnyThread;
import androidx.annotation.Nullable;


/**
 *
 */
@AnyThread
public interface ApiInterceptor {
    /**
     * Will be called when {@link Api} method call
     *
     * @param apiId     id of {@link Api} instance
     * @param methodId  id of callable method
     * @param args      arguments for method, may be null
     */
    void invoke(int apiId, int methodId, @Nullable Object args);
}
