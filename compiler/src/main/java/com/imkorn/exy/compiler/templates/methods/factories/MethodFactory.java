package com.imkorn.exy.compiler.templates.methods.factories;

import androidx.annotation.NonNull;

import com.imkorn.exy.compiler.exceptions.MethodSignatureException;
import com.imkorn.exy.compiler.templates.methods.Method;
import com.imkorn.exy.compiler.utilities.definitions.MethodDefinition;

import java.lang.annotation.Annotation;

import javax.lang.model.element.ExecutableElement;

/**
 * Builds {@link Method}
 */
public abstract class MethodFactory<DefinitionType extends Annotation, MethodTemplateType extends Method<DefinitionType>> {

    MethodFactory(Class<DefinitionType> definitionClazz) {
        this.definitionClazz = definitionClazz;
    }

    public final boolean isAcceptable(@NonNull final ExecutableElement rawMethod) {
        return rawMethod.getAnnotation(definitionClazz) != null;
    }

    public abstract MethodTemplateType build(final int methodId, @NonNull final ExecutableElement rawMethod) throws MethodSignatureException;

    @NonNull
    final MethodDefinition<DefinitionType> getMethodDefinition(@NonNull final ExecutableElement rawMethod) throws MethodSignatureException {

        DefinitionType definition = rawMethod.getAnnotation(definitionClazz);

        if(definition == null) {
            throw new MethodSignatureException("Method template definition not found");
        }

        return MethodDefinition.create(definition);
    }

    private final Class<DefinitionType> definitionClazz;
}
