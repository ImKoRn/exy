package com.imkorn.exy.compiler.templates.methods;

import androidx.annotation.StringDef;

import static com.imkorn.exy.compiler.templates.methods.MethodSuffix.*;

@StringDef({ASYNC, RX_SUBJECT, RX_SINGLE, RX_MAYBE})
public @interface MethodSuffix {
    String RX_SUBJECT = "AsObservable";
    String RX_SINGLE = "AsSingle";
    String RX_MAYBE = "AsMaybe";
    String ASYNC = "Async";
}
