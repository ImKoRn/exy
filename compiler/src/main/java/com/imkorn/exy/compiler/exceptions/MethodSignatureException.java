package com.imkorn.exy.compiler.exceptions;

public class MethodSignatureException extends Exception {
    public MethodSignatureException() {
    }

    public MethodSignatureException(String s) {
        super(s);
    }

    public MethodSignatureException(String s, Throwable throwable) {
        super(s, throwable);
    }

    public MethodSignatureException(Throwable throwable) {
        super(throwable);
    }

    public MethodSignatureException(String s, Throwable throwable, boolean b, boolean b1) {
        super(s, throwable, b, b1);
    }
}
