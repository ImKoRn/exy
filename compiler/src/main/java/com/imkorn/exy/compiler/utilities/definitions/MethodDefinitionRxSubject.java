package com.imkorn.exy.compiler.utilities.definitions;

import com.imkorn.exy.annotations.ApiAccess;
import com.imkorn.exy.annotations.methods.rx.DefRxSubject;

final class MethodDefinitionRxSubject extends MethodDefinition<DefRxSubject> {

    MethodDefinitionRxSubject(DefRxSubject rawDefinition) {
        super(rawDefinition);
    }

    @Override
    public ApiAccess[] getApiAccess() {
        return rawDefinition.access();
    }

}
