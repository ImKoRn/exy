package com.imkorn.exy.compiler.utilities.definitions;

import androidx.annotation.NonNull;

import com.imkorn.exy.annotations.ApiAccess;
import com.imkorn.exy.annotations.methods.DefAsync;
import com.imkorn.exy.annotations.methods.rx.DefRxMaybe;
import com.imkorn.exy.annotations.methods.rx.DefRxSingle;
import com.imkorn.exy.annotations.methods.rx.DefRxSubject;

import java.lang.annotation.Annotation;

import javax.lang.model.element.ExecutableElement;

public abstract class MethodDefinition<DefinitionType extends Annotation> {

    @NonNull
    @SuppressWarnings("unchecked")
    public static <DefinitionType extends Annotation> MethodDefinition<DefinitionType> create(@NonNull DefinitionType rawDefinition) {

        if(rawDefinition instanceof DefAsync) {
            return (MethodDefinition<DefinitionType>) new MethodDefinitionAsync((DefAsync) rawDefinition);
        }
        if(rawDefinition instanceof DefRxMaybe) {
            return (MethodDefinition<DefinitionType>) new MethodDefinitionRxMaybe((DefRxMaybe) rawDefinition);
        }
        if(rawDefinition instanceof DefRxSingle) {
            return (MethodDefinition<DefinitionType>) new MethodDefinitionRxSingle((DefRxSingle) rawDefinition);
        }
        if(rawDefinition instanceof DefRxSubject) {
            return (MethodDefinition<DefinitionType>) new MethodDefinitionRxSubject((DefRxSubject) rawDefinition);
        }

        throw new IllegalArgumentException("Unknown raw definition");
    }

    MethodDefinition(DefinitionType rawDefinition) {
        this.rawDefinition = rawDefinition;
    }

    /**
     * Returns {@link ApiAccess}s for this {@link ExecutableElement}
     */
    public abstract ApiAccess[] getApiAccess();

    @NonNull
    public final DefinitionType getRawDefinition() {
        return rawDefinition;
    }

    final DefinitionType rawDefinition;
}
