package com.imkorn.exy.compiler.utilities;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.squareup.javapoet.AnnotationSpec;
import com.squareup.javapoet.ParameterSpec;
import com.squareup.javapoet.TypeName;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import javax.lang.model.element.AnnotationMirror;
import javax.lang.model.element.Element;
import javax.lang.model.element.Modifier;
import javax.lang.model.element.VariableElement;
import javax.lang.model.type.MirroredTypeException;
import javax.lang.model.type.TypeMirror;
import javax.lang.model.util.Types;

/**
 * Helper for types
 */
public class TypeUtils {
    private TypeUtils() {
        //no instance
    }

    /**
     * Filter SomeType.class to TypeMirror
     */
    public static TypeMirror filterTypeMirror(Runnable runnable) {
        try {
            runnable.run();
            throw new IllegalStateException("Illegal no exception when access to annotation Type parameter");
        } catch(MirroredTypeException m) {
            return m.getTypeMirror();
        }
    }

    /**
     * Wrap primitive type to it boxed representation if needed
     */
    public static TypeMirror protectType(Types types, TypeMirror typeMirror) {

        if(typeMirror.getKind()
                     .isPrimitive()) {
            return types.boxedClass(types.getPrimitiveType(typeMirror.getKind()))
                        .asType();
        }

        return typeMirror;
    }

    @NonNull
    public static Collection<? extends TypeName> asNames(@Nullable Collection<? extends TypeMirror> types) {
        if(types == null || types.isEmpty()) {
            return Collections.emptyList();
        }

        Collection<TypeName> names = new LinkedList<>();
        for(TypeMirror type : types) {
            names.add(TypeName.get(type));
        }
        return names;
    }

    /**
     * Filter same hierarchy of exceptions
     */
    public static List<? extends TypeMirror> filterExceptions(final List<? extends TypeMirror> exceptions, Types types) {

        final List<? extends TypeMirror> copy = new ArrayList<>(exceptions);
        final List<TypeMirror> result = new LinkedList<>();
        for(int outIndex = 0; outIndex < copy.size(); ) {
            final TypeMirror out = copy.get(outIndex);

            boolean next = true;
            for(int inIndex = outIndex + 1; inIndex < copy.size(); ) {
                final TypeMirror in = copy.get(inIndex);

                // out is subtype of in
                if(types.isSubtype(out, in)) {
                    next = false;
                    break;
                }

                // in is subtype of out
                if(types.isSubtype(in, out)) {
                    copy.remove(inIndex);
                    continue;
                }

                inIndex++;
            }

            if(next) {
                result.add(out);
                outIndex++;
            } else {
                copy.remove(outIndex);
            }
        }

        return result;
    }

    public static List<AnnotationSpec> getAnnotations(@NonNull Element element) {
        final Collection<? extends AnnotationMirror> annotationMirrors = element.getAnnotationMirrors();

        if(annotationMirrors.isEmpty()) {
            return Collections.emptyList();
        }
        List<AnnotationSpec> wrapperAnnotations = new ArrayList<>(annotationMirrors.size());
        for(AnnotationMirror annotationMirror : annotationMirrors) {
            wrapperAnnotations.add(AnnotationSpec.get(annotationMirror));
        }
        return wrapperAnnotations;
    }

    public static List<ParameterSpec> getParameters(final List<? extends VariableElement> declaredParameters) {

        if(declaredParameters.isEmpty()) {
            return Collections.emptyList();
        }

        List<ParameterSpec> parameters = new ArrayList<>(declaredParameters.size());
        for(int index = 0; index < declaredParameters.size(); index++) {
            final VariableElement variableElement = declaredParameters.get(index);

            final Collection<AnnotationSpec> wrapperAnnotations = TypeUtils.getAnnotations(variableElement);

            parameters.add(ParameterSpec.builder(TypeName.get(variableElement.asType()),
                                                 variableElement.getSimpleName()
                                                                .toString(),
                                                 Modifier.FINAL)
                                        .addAnnotations(wrapperAnnotations)
                                        .build());
        }
        return parameters;
    }
}
