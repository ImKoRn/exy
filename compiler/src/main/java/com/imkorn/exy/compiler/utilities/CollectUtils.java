package com.imkorn.exy.compiler.utilities;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.imkorn.exy.annotations.DefMappable;
import com.imkorn.exy.annotations.routes.DefRoute;
import com.imkorn.exy.annotations.routes.DefRouter;
import com.imkorn.exy.compiler.exceptions.RouterDeclarationException;

import java.util.AbstractMap;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.Modifier;
import javax.lang.model.element.TypeElement;
import javax.lang.model.type.TypeKind;
import javax.lang.model.type.TypeMirror;
import javax.lang.model.util.Elements;
import javax.lang.model.util.Types;

/**
 * Helper for collecting items
 */
public class CollectUtils {
    public static List<ExecutableElement> collectMethods(Types typesUtils, Elements elementsUtils, TypeElement mappable, DefMappable config) {
        List<ExecutableElement> rootMethods = new LinkedList<>();

        for(Element element : mappable.getEnclosedElements()) {
            final ExecutableElement method = asMethod(element);
            if(method != null) {
                rootMethods.add(method);
            }
        }

        if(!config.deepMapping()) {
            return rootMethods;
        }

        final Map<String, Collection<ExecutableElement>> methodsMap = new HashMap<>();

        for(ExecutableElement method : rootMethods) {
            final LinkedList<ExecutableElement> elements = new LinkedList<>();

            elements.add(method);
            methodsMap.put(method.getSimpleName()
                                 .toString(), elements);
        }

        TypeElement typeElement = mappable;
        for(; ; ) {
            final TypeMirror superclass = typeElement.getSuperclass();

            if(superclass.getKind() == TypeKind.NONE) {
                break;
            }

            typeElement = (TypeElement) typesUtils.asElement(superclass);

            for(Element element : typeElement.getEnclosedElements()) {
                final ExecutableElement method = asMethod(element);
                if(method == null) {
                    continue;
                }

                final String name = method.getSimpleName()
                                          .toString();

                final Collection<ExecutableElement> similarMethods = methodsMap.get(name);

                if(similarMethods == null) {
                    Collection<ExecutableElement> elements = new LinkedList<>();
                    elements.add(method);
                    methodsMap.put(name, elements);
                } else {
                    boolean independent = true;
                    for(ExecutableElement similarMethod : similarMethods) {
                        if(elementsUtils.overrides(similarMethod, method, typeElement)) {
                            independent = false;
                            break;
                        }
                    }

                    if(independent) {
                        similarMethods.add(method);
                    }
                }
            }
        }
        List<ExecutableElement> methods = new LinkedList<>();

        for(Map.Entry<?, Collection<ExecutableElement>> entry : methodsMap.entrySet()) {
            methods.addAll(entry.getValue());
        }

        return methods;
    }

    @Nullable
    private static ExecutableElement asMethod(Element element) {
        // Check precondition
        if(element.getKind() != ElementKind.METHOD) {
            return null;
        }

        final ExecutableElement method = (ExecutableElement) element;

        // Check precondition
        final Set<Modifier> modifiers = method.getModifiers();
        if((modifiers.contains(Modifier.PRIVATE) || modifiers.contains(Modifier.PROTECTED) || modifiers.contains(Modifier.STATIC))) {
            return null;
        }
        return method;
    }

    public static Collection<Map.Entry<Element, DefRoute>> collectRoutes(Types typesUtils, DefRouter routerDef) throws RouterDeclarationException {
        final DefRoute[] routesDef = routerDef.value();

        if(routesDef.length == 0) {
            return Collections.emptyList();
        }

        Collection<Map.Entry<Element, DefRoute>> routes = new LinkedList<>();

        for(DefRoute routeDef : routesDef) {
            routes.add(new AbstractMap.SimpleImmutableEntry<>(getRoute(typesUtils, routeDef),
                                                              routeDef));
        }

        return routes;
    }

    @NonNull
    private static Element getRoute(Types typesUtils, @NonNull final DefRoute routeDef) throws RouterDeclarationException {
        final TypeMirror route = TypeUtils.filterTypeMirror(routeDef::route);

        final Element routeElement = typesUtils.asElement(route);

        if(routeElement.getModifiers()
                       .contains(Modifier.ABSTRACT)) {
            throw new RouterDeclarationException("Route must not be abstract");
        }

        if(routeElement.getEnclosingElement() != null &&
           !routeElement.getModifiers().contains(Modifier.STATIC)) {
            throw new RouterDeclarationException("Route not accessible");
        }

        return routeElement;
    }

    private CollectUtils() {
        //no instance
    }
}
