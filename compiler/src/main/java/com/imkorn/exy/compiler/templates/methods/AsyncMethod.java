package com.imkorn.exy.compiler.templates.methods;

import androidx.annotation.NonNull;

import com.imkorn.exy.annotations.methods.DefAsync;
import com.imkorn.exy.compiler.utilities.TypeUtils;
import com.imkorn.exy.compiler.utilities.definitions.MethodDefinition;
import com.squareup.javapoet.CodeBlock;
import com.squareup.javapoet.MethodSpec;
import com.squareup.javapoet.TypeName;

import java.util.List;

import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.VariableElement;

/**
 * Async api generation without transport
 */
public final class AsyncMethod extends Method<DefAsync> {

    public AsyncMethod(final MethodDefinition<DefAsync> methodDefinition, final int methodId, @NonNull ExecutableElement rawMethod) {
        super(methodDefinition, methodId, createName(rawMethod, MethodSuffix.ASYNC), rawMethod);
        create();
    }

    @Override
    protected void createBody(@NonNull MethodSpec.Builder builder) {
        builder.addCode(createArgs(getRawMethod().getParameters()))
               .addStatement("$N($L, $L)", METHOD_INVOKE, getMethodId(), VALUE_NAME_ARGS);
    }

    @Override
    protected void createSignature(@NonNull MethodSpec.Builder builder) {
        super.createSignature(builder);
        builder.addParameters(TypeUtils.getParameters(getRawMethod().getParameters()))
               .returns(TypeName.VOID);
    }

    private CodeBlock createArgs(final List<? extends VariableElement> parameters) {

        final int size = parameters.size();

        switch(size) {
            case 0: {
                return CodeBlock.of("$T $L = null;\n", Object.class, VALUE_NAME_ARGS);
            }
            case 1: {
                return CodeBlock.of("$T $L = $L;\n",
                                    TypeName.OBJECT,
                                    VALUE_NAME_ARGS,
                                    parameters.get(0)
                                              .getSimpleName()
                                              .toString());
            }
            default: {
                final CodeBlock.Builder builder = CodeBlock.builder()
                                                           .addStatement("$T[] $L = new $T[$L]",
                                                                         TypeName.OBJECT,
                                                                         VALUE_NAME_ARGS,
                                                                         TypeName.OBJECT,
                                                                         size);
                int index = 0;
                for(VariableElement parameter : parameters) {
                    builder.addStatement("$L[$L] = $L",
                                         VALUE_NAME_ARGS,
                                         index++,
                                         parameter.getSimpleName()
                                                  .toString());
                }

                return builder.build();
            }
        }
    }
}
