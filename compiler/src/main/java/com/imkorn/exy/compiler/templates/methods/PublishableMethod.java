package com.imkorn.exy.compiler.templates.methods;

import androidx.annotation.NonNull;

import com.imkorn.exy.compiler.utilities.definitions.MethodDefinition;
import com.squareup.javapoet.CodeBlock;
import com.squareup.javapoet.MethodSpec;
import com.squareup.javapoet.TypeName;

import java.lang.annotation.Annotation;
import java.util.Collections;
import java.util.List;

import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.VariableElement;

/**
 * Defines generation behavior for transports
 */
public abstract class PublishableMethod<DefinitionType extends Annotation> extends Method<DefinitionType> {

    protected PublishableMethod(@NonNull final MethodDefinition<DefinitionType> methodDefinition,
                                final int methodId,
                                @NonNull String methodName,
                                @NonNull final ExecutableElement rawMethod) {
        super(methodDefinition, methodId, methodName, rawMethod);
    }

    /**
     * In this method must be defined value:
     * <ul>
     * <li>{@value VALUE_NAME_TRANSPORT}</li>
     * <li>{@value VALUE_NAME_PUBLISHER}</li>
     * </ul>
     */
    protected abstract CodeBlock createCode(@NonNull MethodSpec.Builder builder);

    @Override
    protected void createBody(@NonNull MethodSpec.Builder builder) {
        builder.addCode(createCode(builder))
               .addCode(createArgs(getParameters(getRawMethod().getParameters())))
               .addStatement("$N($L, $L)", METHOD_INVOKE, getMethodId(), VALUE_NAME_ARGS)
               .addStatement("return $L", VALUE_NAME_TRANSPORT);
    }

    private List<? extends VariableElement> getParameters(List<? extends VariableElement> parameters) {
        return parameters.size() == 1 ? Collections.emptyList() : parameters.subList(1, parameters.size());
    }

    private CodeBlock createArgs(final List<? extends VariableElement> parameters) {

        final int size = parameters.size() + 1;

        switch(size) {
            case 1: {
                return CodeBlock.of("$T $L = $L;\n", TypeName.OBJECT, VALUE_NAME_ARGS, VALUE_NAME_PUBLISHER);
            }
            default: {
                final CodeBlock.Builder builder = CodeBlock.builder()
                                                           .addStatement("$T[] $L = new $T[$L]",
                                                                         TypeName.OBJECT,
                                                                         VALUE_NAME_ARGS,
                                                                         TypeName.OBJECT,
                                                                         size)
                                                           .addStatement("$L[$L] = $L", VALUE_NAME_ARGS, 0, VALUE_NAME_PUBLISHER);
                int index = 1;
                for(VariableElement parameter : parameters) {
                    builder.addStatement("$L[$L] = $L",
                                         VALUE_NAME_ARGS,
                                         index++,
                                         parameter.getSimpleName()
                                                  .toString());
                }

                return builder.build();
            }
        }
    }

    protected static final String VALUE_NAME_TRANSPORT = "exy_Transport";
    protected static final String VALUE_NAME_PUBLISHER = "exy_Publisher";
}
