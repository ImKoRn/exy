package com.imkorn.exy.compiler.templates.methods.factories;

import androidx.annotation.NonNull;

import com.imkorn.exy.annotations.methods.DefAsync;
import com.imkorn.exy.compiler.exceptions.MethodSignatureException;
import com.imkorn.exy.compiler.templates.methods.AsyncMethod;

import javax.lang.model.element.ExecutableElement;

public final class AsyncMethodFactory extends MethodFactory<DefAsync, AsyncMethod> {

    public AsyncMethodFactory() {
        super(DefAsync.class);
    }

    @Override
    public AsyncMethod build(int methodId, @NonNull ExecutableElement rawMethod) throws MethodSignatureException {
        return new AsyncMethod(getMethodDefinition(rawMethod), methodId, rawMethod);
    }
}
