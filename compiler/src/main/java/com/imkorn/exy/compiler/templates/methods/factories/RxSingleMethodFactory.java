package com.imkorn.exy.compiler.templates.methods.factories;

import androidx.annotation.NonNull;

import com.imkorn.exy.annotations.methods.rx.DefRxSingle;
import com.imkorn.exy.compiler.exceptions.MethodSignatureException;
import com.imkorn.exy.compiler.templates.methods.rx.RxMethod;

import javax.lang.model.element.ExecutableElement;
import javax.lang.model.util.Elements;
import javax.lang.model.util.Types;

public final class RxSingleMethodFactory extends MethodFactory<DefRxSingle, RxMethod<DefRxSingle>> {

    public RxSingleMethodFactory(@NonNull final Elements elements, @NonNull final Types types) {
        super(DefRxSingle.class);
        this.elements = elements;
        this.types = types;
    }

    @Override
    public RxMethod<DefRxSingle> build(int methodId, @NonNull ExecutableElement rawMethod) throws MethodSignatureException {
        return RxMethod.createRxSingleMethod(getMethodDefinition(rawMethod), methodId, rawMethod, types, elements);
    }

    @NonNull
    private final Elements elements;
    @NonNull
    private final Types types;
}
