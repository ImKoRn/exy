package com.imkorn.exy.compiler.utilities;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

/**
 * Helper for class, fields, variables names
 */
public class NameUtils {

    @NonNull
    public static String getClassBaseName(@NonNull String name) {
        final StringBuilder builder = new StringBuilder(name);
        for(int index = builder.length() - 1; index >= 0; index--)
        {
            if(builder.charAt(index) == '.') builder.deleteCharAt(index);
        }
        return builder.toString();
    }

    @NonNull
    public static String asFieldName(@NonNull String base) {
        final char firstSymbol = Character.toLowerCase(base.charAt(0));

        if(base.length() == 1) {
            return String.valueOf(firstSymbol);
        }

        return firstSymbol + base.substring(1, base.length());
    }

    @NonNull
    public static String asStaticFieldName(@NonNull String base) {
        return asStaticFieldName(base, null);
    }

    @NonNull
    public static String asStaticFieldName(@NonNull String base, @Nullable String prefix) {
        final int rawCapacity = base.length() + (prefix != null ? prefix.length() : 0);

        StringBuilder builder = new StringBuilder((int) (rawCapacity * 1.5f));

        if(prefix != null) {
            builder.append(prefix);
        }

        boolean word = false;
        for(int index = 0; index < base.length(); index++) {

            final char currChar = base.charAt(index);

            final char upChar = Character.toUpperCase(currChar);

            final boolean isUpperCase = Character.isUpperCase(currChar);
            if(isUpperCase && word) {
                word = false;
                builder.append('_')
                       .append(upChar);
            } else {
                word = !isUpperCase;
                builder.append(upChar);
            }
        }

        return builder.toString();
    }

    private NameUtils() {
        //no instance
    }
}
