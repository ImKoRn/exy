package com.imkorn.exy.compiler.utilities.definitions;

import com.imkorn.exy.annotations.ApiAccess;
import com.imkorn.exy.annotations.methods.rx.DefRxMaybe;

final class MethodDefinitionRxMaybe extends MethodDefinition<DefRxMaybe> {

    MethodDefinitionRxMaybe(DefRxMaybe rawDefinition) {
        super(rawDefinition);
    }

    @Override
    public ApiAccess[] getApiAccess() {
        return rawDefinition.access();
    }

}
