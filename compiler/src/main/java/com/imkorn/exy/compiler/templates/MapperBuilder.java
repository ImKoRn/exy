package com.imkorn.exy.compiler.templates;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.imkorn.exy.annotations.ApiAccess;
import com.imkorn.exy.annotations.DefMappable;
import com.imkorn.exy.annotations.routes.DefRoute;
import com.imkorn.exy.annotations.routes.DefRouter;
import com.imkorn.exy.compiler.exceptions.MapperBuildException;
import com.imkorn.exy.compiler.exceptions.MethodSignatureException;
import com.imkorn.exy.compiler.exceptions.RouterDeclarationException;
import com.imkorn.exy.compiler.templates.api.ApiTemplate;
import com.imkorn.exy.compiler.templates.methods.Method;
import com.imkorn.exy.compiler.templates.methods.factories.MethodFactory;
import com.imkorn.exy.compiler.utilities.CollectUtils;
import com.imkorn.exy.compiler.utilities.NameUtils;
import com.imkorn.exy.mapping.Mapper;
import com.imkorn.exy.mapping.Router;
import com.imkorn.exy.mapping.Routes;
import com.imkorn.exy.mapping.ApiInterceptor;
import com.squareup.javapoet.ClassName;
import com.squareup.javapoet.CodeBlock;
import com.squareup.javapoet.FieldSpec;
import com.squareup.javapoet.MethodSpec;
import com.squareup.javapoet.ParameterSpec;
import com.squareup.javapoet.ParameterizedTypeName;
import com.squareup.javapoet.TypeName;
import com.squareup.javapoet.TypeSpec;
import com.squareup.javapoet.WildcardTypeName;

import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.lang.model.element.Element;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.Modifier;
import javax.lang.model.element.TypeElement;
import javax.lang.model.element.VariableElement;
import javax.lang.model.util.Types;

/**
 * Build mapper
 */
public class MapperBuilder {

    public MapperBuilder(@NonNull Types typesUtils, @NonNull TypeElement mappable, @NonNull DefMappable mappableDef) {
        this.typesUtils = typesUtils;
        this.mappable = mappable;
        this.mappableDef = mappableDef;
    }

    /**
     * Build {@link Method} using {@link MethodFactory} from {@link ExecutableElement} and internal method id.
     * If built successful add result to this builder methods
     */
    public void addMethod(@NonNull ExecutableElement method, @NonNull MethodFactory methodFactory) throws MethodSignatureException {

        final int methodId = methods.size();

        final Method methodTemplate = methodFactory.build(methodId, method);

        methods.add(methodTemplate);

        for(ApiAccess access : methodTemplate.getMethodDefinition()
                                             .getApiAccess()) {
            switch(access) {
                case PUBLIC: {
                    publicMethods.add(methodTemplate.getMethod());
                    break;
                }
                case CHILD: {
                    childMethods.add(methodTemplate.getMethod());
                    break;
                }
                case SELF: {
                    selfMethods.add(methodTemplate.getMethod());
                    break;
                }
                case ROUTE: {
                    routeMethods.add(methodTemplate.getMethod());
                    break;
                }
            }
        }
    }

    /**
     * Build {@link Mapper} from previously added {@link Method}s
     */
    public TypeSpec build(@NonNull String packageName) throws MapperBuildException {
        final String customName = mappableDef.name();
        final String baseName = customName.isEmpty()?
                                mappable.getSimpleName().toString() + MAPPER_SUFFIX :
                                customName;
        final String deployPackage = packageName + '.' + baseName;
        final ClassName mapperDirectType = ClassName.get(packageName, baseName);
        final ApiTemplate[] apiTemplates = createApi(mapperDirectType, deployPackage);
        final ParameterizedTypeName mapperType = ParameterizedTypeName.get(ClassName.get(Mapper.class),
                                                                           TypeName.get(mappable.asType()),
                                                                           apiTemplates[0].getTypeName(),
                                                                           apiTemplates[1].getTypeName(),
                                                                           apiTemplates[2].getTypeName());

        final TypeSpec.Builder builder = TypeSpec.classBuilder(baseName)
                                                 .addSuperinterface(mapperType)
                                                 .addModifiers(Modifier.PUBLIC, Modifier.FINAL);


        builder.addMethod(MethodSpec.constructorBuilder()
                                     .addModifiers(Modifier.PRIVATE)
                                     .build())
               .addField(FieldSpec.builder(mapperDirectType,
                                           MAPPER_INSTANCE_FIELD_NAME,
                                           Modifier.PRIVATE, Modifier.STATIC, Modifier.FINAL)
                                  .initializer("new $T()", mapperDirectType)
                                  .build())
               .addMethod(MethodSpec.methodBuilder("get")
                                    .addModifiers(Modifier.PUBLIC, Modifier.STATIC)
                                    .addStatement("return $L", MAPPER_INSTANCE_FIELD_NAME)
                                    .returns(mapperDirectType)
                                    .build());

        if(mappableDef.route()) {
            builder.addType(ApiTemplate.createRoute(mapperDirectType, routeMethods));
        }

        for(final ApiTemplate apiTemplate : apiTemplates) {
            builder.addType(apiTemplate.build())
                   .addMethod(apiTemplate.buildCreateMethod());
        }

        // If not router don't extend
        final DefRouter routerDef = mappable.getAnnotation(DefRouter.class);
        if(routerDef != null) {
            implementRouter(builder, routerDef, deployPackage);
        }

        return builder.addMethod(generateMapMethod(mappable))
                      .build();

    }

    /**
     * Makes {@link Mapper} implement {@link Router}
     */
    private void implementRouter(@NonNull TypeSpec.Builder builder,
                                 @NonNull DefRouter routerDef,
                                 @NonNull String deployPackage) throws RouterDeclarationException {
        final TypeSpec routesTypeSpec = generateRoutesType(routerDef);

        final TypeName routesTypeName = ClassName.get(deployPackage, routesTypeSpec.name);

        final ParameterizedTypeName routerType = ParameterizedTypeName.get(ClassName.get(Router.class), routesTypeName);

        final MethodSpec createRoutesMethodSpec = generateRoutesMethod(routesTypeName);

        builder.addSuperinterface(routerType)
               .addType(routesTypeSpec)
               .addMethod(createRoutesMethodSpec);
    }

    @NonNull
    private ApiTemplate[] createApi(@NonNull final ClassName mapperType, @NonNull final String deployPackage) {
        return new ApiTemplate[]{ApiTemplate.createPublic(mapperType, deployPackage, publicMethods),
                                 ApiTemplate.createChild(mapperType, deployPackage, childMethods),
                                 ApiTemplate.createSelf(mapperType, deployPackage, selfMethods)};
    }

    /**
     * Generate {@link Mapper#map(Object, int, Object)}
     */
    @NonNull
    private MethodSpec generateMapMethod(@NonNull TypeElement mappable) {
        return MethodSpec.methodBuilder(MAP_METHOD_NAME)
                         .addAnnotation(Override.class)
                         .addModifiers(Modifier.PUBLIC, Modifier.FINAL)
                         .addException(Throwable.class)
                         .addParameter(ParameterSpec.builder(TypeName.get(mappable.asType()), PARAMETER_MAPPABLE, Modifier.FINAL)
                                                    .addAnnotation(NonNull.class)
                                                    .build())
                         .addParameter(TypeName.INT, PARAMETER_METHOD_ID, Modifier.FINAL)
                         .addParameter(ParameterSpec.builder(TypeName.OBJECT, MAP_METHOD_ARGS_PARAMETER, Modifier.FINAL)
                                                    .addAnnotation(Nullable.class)
                                                    .build())
                         .addCode(generateMapMethodCode())
                         .returns(TypeName.VOID)
                         .build();
    }

    /**
     * Generate all code for {@link Mapper#map(Object, int, Object)}
     */
    @NonNull
    private CodeBlock generateMapMethodCode() {

        if(methods.isEmpty()) {
            return CodeBlock.of("// Empty\n");
        }

        if(methods.size() == 1) {
            final Method method = methods.get(0);
            return CodeBlock.builder()
                            .beginControlFlow("if($L == $L)", PARAMETER_METHOD_ID, method.getMethodId())
                            .add(generateMapCall(method.getRawMethod()))
                            .endControlFlow()
                            .build();
        }

        final CodeBlock.Builder builder = CodeBlock.builder()
                                                   .beginControlFlow("switch($L)", PARAMETER_METHOD_ID);
        for(int index = 0; index < methods.size(); index++) {
            final Method method = methods.get(index);

            builder.beginControlFlow("case $L:", method.getMethodId())
                   .add(generateMapCall(method.getRawMethod()))
                   .endControlFlow();
        }

        return builder.endControlFlow().build();
    }

    @NonNull
    private TypeSpec generateRoutesType(@NonNull DefRouter routerDef) throws RouterDeclarationException {

        final TypeSpec.Builder routesBuilder = TypeSpec.classBuilder(ROUTES_NAME)
                                                       .addSuperinterface(ClassName.get(Routes.class));

        final CodeBlock.Builder constructorCode = CodeBlock.builder();

        final Collection<Map.Entry<Element, DefRoute>> routes = CollectUtils.collectRoutes(typesUtils, routerDef);
        final Map<String, String> boundedRoutes = new HashMap<>(routes.size());

        int nextRouteId = 0;
        for(Map.Entry<Element, DefRoute> route : routes) {
            final int routeId = nextRouteId++;

            final String routeName = route.getKey().getSimpleName().toString();
            final TypeName fieldType = ClassName.bestGuess(routeName);
            final String customName = route.getValue().name();
            final String baseName = customName.isEmpty()? NameUtils.getClassBaseName(routeName) : customName;
            final String staticFieldBaseName = NameUtils.asStaticFieldName(baseName, ID_PREFIX);
            final String fieldBaseName = NameUtils.asFieldName(baseName);

            final String methodName;
            final String staticFieldName;
            final String fieldName;
            for(int index = 0;;) {
                final String key = index == 0? staticFieldBaseName : staticFieldBaseName + index;
                if(boundedRoutes.containsKey(key)) {
                    index++;
                } else {
                    staticFieldName = key;
                    methodName = METHOD_GET_PREFIX + (index == 0? baseName : baseName + index);
                    fieldName = index == 0? fieldBaseName : fieldBaseName + index;
                    boundedRoutes.put(staticFieldName, fieldName);
                    break;
                }
            }

            // Add id field
            routesBuilder.addField(FieldSpec.builder(TypeName.INT, staticFieldName, Modifier.PUBLIC, Modifier.STATIC, Modifier.FINAL)
                                            .initializer("$L", routeId)
                                            .build());
            // Add field
            routesBuilder.addField(fieldType, fieldName, Modifier.PRIVATE, Modifier.FINAL);

            // Add initialization
            constructorCode.addStatement("$L = new $T($L, $L)",
                                         fieldName,
                                         fieldType,
                                         staticFieldName,
                                         PARAMETER_API_INTERCEPTOR);
            // Add get method
            routesBuilder.addMethod(MethodSpec.methodBuilder(methodName)
                                              .addCode(CodeBlock.of("return $L;\n", fieldName))
                                              .addModifiers(Modifier.PUBLIC, Modifier.FINAL)
                                              .returns(fieldType)
                                              .build());
        }

        final MethodSpec constructor = MethodSpec.constructorBuilder()
                                                 .addModifiers(Modifier.PRIVATE)
                                                 .addParameter(ParameterSpec.builder(ClassName.get(ApiInterceptor.class),
                                                                                     PARAMETER_API_INTERCEPTOR,
                                                                                     Modifier.FINAL)
                                                                            .addAnnotation(NonNull.class)
                                                                            .build())
                                                 .addCode(constructorCode.build())
                                                 .build();

        return routesBuilder.addMethod(constructor)
                            .addMethod(generateGetCountMethod(routes.size()))
                            .addMethod(generateGetRouteMapper(boundedRoutes))
                            .addModifiers(Modifier.PUBLIC, Modifier.FINAL)
                            .build();
    }

    @NonNull
    private MethodSpec generateGetRouteMapper(@NonNull final Map<String, String> boundedRoutes) {

        CodeBlock.Builder builder = CodeBlock.builder()
                                             .beginControlFlow("switch($L)",
                                                               PARAMETER_ROUTE_ID);
        for(Map.Entry<String, String> bound : boundedRoutes.entrySet()) {
            builder.addStatement("case $L: return $L.$L()",
                                 bound.getKey(),
                                 bound.getValue(),
                                 METHOD_GET_MAPPER);
        }
        builder.addStatement("default: return null")
               .endControlFlow();

        final WildcardTypeName wildcardTypeName = WildcardTypeName.subtypeOf(Object.class);
        final ParameterizedTypeName rawMapper = ParameterizedTypeName.get(ClassName.get(Mapper.class),
                                                                          wildcardTypeName,
                                                                          wildcardTypeName,
                                                                          wildcardTypeName,
                                                                          wildcardTypeName);

        return MethodSpec.methodBuilder(METHOD_GET_ROUTE_MAPPER)
                         .addModifiers(Modifier.PUBLIC, Modifier.FINAL)
                         .addAnnotation(Override.class)
                         .addParameter(int.class, PARAMETER_ROUTE_ID, Modifier.FINAL)
                         .addCode(builder.build())
                         .returns(ParameterizedTypeName.get(ClassName.get(Class.class),
                                                            WildcardTypeName.subtypeOf(rawMapper)))
                         .build();
    }

    @NonNull
    private MethodSpec generateGetCountMethod(final int size) {
        return MethodSpec.methodBuilder(METHOD_GET_COUNT)
                         .addModifiers(Modifier.PUBLIC, Modifier.FINAL)
                         .addAnnotation(Override.class)
                         .addStatement("return $L", size)
                         .returns(int.class)
                         .build();
    }

    @NonNull
    private MethodSpec generateRoutesMethod(@NonNull final TypeName routesTypeName) {

        return MethodSpec.methodBuilder(METHOD_CREATE_ROUTES)
                         .addAnnotation(NonNull.class)
                         .addModifiers(Modifier.PUBLIC, Modifier.FINAL)
                         .addParameter(ParameterSpec.builder(ClassName.get(ApiInterceptor.class), PARAMETER_API_INTERCEPTOR,
                                                             Modifier.FINAL)
                                                    .addAnnotation(NonNull.class)
                                                    .build())
                         .addStatement("return new $T($L)",
                                       routesTypeName, PARAMETER_API_INTERCEPTOR)
                         .returns(routesTypeName)
                         .build();
    }

    /**
     * Generate mapping to raw method call
     */
    @NonNull
    private CodeBlock generateMapCall(@NonNull ExecutableElement method) {
        final List<? extends VariableElement> originalParameters = method.getParameters();

        if(originalParameters.isEmpty()) {
            return CodeBlock.of("$L.$N();\n", PARAMETER_MAPPABLE, method.getSimpleName());
        }

        final VariableElement publisherParameter = originalParameters.get(originalParameters.size() - 1);

        if(originalParameters.size() == 1) {
            return CodeBlock.of("$L.$N(($T) $L);\n", PARAMETER_MAPPABLE, method.getSimpleName(), publisherParameter, MAP_METHOD_ARGS_PARAMETER);
        }


        CodeBlock.Builder builder = CodeBlock.builder()
                                             .addStatement("$T[] $L = ($T[]) $L",
                                                           TypeName.OBJECT,
                                                           MAP_METHOD_ARGS_ARR_VARIABLE,
                                                           TypeName.OBJECT,
                                                           MAP_METHOD_ARGS_PARAMETER)
                                             .add("$L.$N(", PARAMETER_MAPPABLE, method.getSimpleName());

        for(int index = 0; index < originalParameters.size(); index++) {
            final VariableElement parameter = originalParameters.get(index);

            final boolean isLast = index == originalParameters.size() - 1;

            builder.add("($T) $L[$L]" + (isLast ? ");\n" : ", "), parameter.asType(), MAP_METHOD_ARGS_ARR_VARIABLE, index);
        }

        return builder.build();
    }

    private final Types typesUtils;
    private final TypeElement mappable;
    private final DefMappable mappableDef;

    private final List<Method> methods = new LinkedList<>();

    private final List<MethodSpec> publicMethods = new LinkedList<>();
    private final List<MethodSpec> childMethods = new LinkedList<>();
    private final List<MethodSpec> selfMethods = new LinkedList<>();
    private final List<MethodSpec> routeMethods = new LinkedList<>();

    private static final String MAPPER_INSTANCE_FIELD_NAME = "instance";
    private static final String MAP_METHOD_NAME = "map";
    private static final String PARAMETER_MAPPABLE = "mappable";
    private static final String PARAMETER_METHOD_ID = "methodId";
    private static final String MAP_METHOD_ARGS_PARAMETER = "args";
    private static final String MAP_METHOD_ARGS_ARR_VARIABLE = "argsArr";
    private static final String MAPPER_SUFFIX = "Mapper";

    // Router constants
    private static final String ROUTES_NAME = "Routes";
    private static final String PARAMETER_API_INTERCEPTOR = "apiInterceptor";
    private static final String PARAMETER_ROUTE_ID = "routeId";
    private static final String ID_PREFIX = "ID_";
    private static final String METHOD_CREATE_ROUTES = "createRoutes";
    private static final String METHOD_GET_COUNT = "getCount";
    private static final String METHOD_GET_ROUTE_MAPPER = "getRouteMapper";
    private static final String METHOD_GET_MAPPER = "getMapper";

    private static final String METHOD_GET_PREFIX = "get";
}
