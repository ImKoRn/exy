package com.imkorn.exy.compiler.templates.methods.rx;


import androidx.annotation.NonNull;

import com.imkorn.exy.annotations.methods.rx.DefRxMaybe;
import com.imkorn.exy.annotations.methods.rx.DefRxSingle;
import com.imkorn.exy.annotations.methods.rx.DefRxSubject;
import com.imkorn.exy.annotations.methods.rx.RxSubject;
import com.imkorn.exy.compiler.exceptions.MethodSignatureException;
import com.imkorn.exy.compiler.templates.methods.MethodSuffix;
import com.imkorn.exy.compiler.templates.methods.PublishableMethod;
import com.imkorn.exy.compiler.utilities.TypeUtils;
import com.imkorn.exy.compiler.utilities.definitions.MethodDefinition;
import com.squareup.javapoet.ClassName;
import com.squareup.javapoet.CodeBlock;
import com.squareup.javapoet.MethodSpec;
import com.squareup.javapoet.ParameterizedTypeName;
import com.squareup.javapoet.TypeName;

import java.lang.annotation.Annotation;
import java.util.Collections;
import java.util.List;

import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.TypeElement;
import javax.lang.model.element.VariableElement;
import javax.lang.model.type.TypeKind;
import javax.lang.model.type.TypeMirror;
import javax.lang.model.util.Elements;
import javax.lang.model.util.Types;

import io.reactivex.Maybe;
import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.Single;
import io.reactivex.subjects.AsyncSubject;
import io.reactivex.subjects.BehaviorSubject;
import io.reactivex.subjects.MaybeSubject;
import io.reactivex.subjects.PublishSubject;
import io.reactivex.subjects.ReplaySubject;
import io.reactivex.subjects.SingleSubject;
import io.reactivex.subjects.Subject;
import io.reactivex.subjects.UnicastSubject;

/**
 * Base rx method wrapper
 */
public final class RxMethod<DefinitionType extends Annotation> extends PublishableMethod<DefinitionType> {

    public static RxMethod<DefRxMaybe>
    createRxMaybeMethod(@NonNull final MethodDefinition<DefRxMaybe> methodDefinition,
                        final int methodId,
                        @NonNull final ExecutableElement rawMethod,
                        @NonNull final Types types,
                        @NonNull final Elements elements) throws MethodSignatureException {
        return new RxMethod<>(methodDefinition,
                              methodId,
                              createName(rawMethod, MethodSuffix.RX_MAYBE),
                              rawMethod,
                              Maybe.class,
                              MaybeSubject.class,
                              types,
                              elements,
                              TypeUtils.filterTypeMirror(methodDefinition.getRawDefinition()::value));
    }

    public static RxMethod<DefRxSingle>
    createRxSingleMethod(@NonNull final MethodDefinition<DefRxSingle> methodDefinition,
                         final int methodId,
                         @NonNull final ExecutableElement rawMethod,
                         @NonNull final Types types,
                         @NonNull final Elements elements) throws MethodSignatureException {
        return new RxMethod<>(methodDefinition,
                              methodId,
                              createName(rawMethod, MethodSuffix.RX_SINGLE),
                              rawMethod,
                              Single.class,
                              SingleSubject.class,
                              types,
                              elements,
                              TypeUtils.filterTypeMirror(methodDefinition.getRawDefinition()::value));
    }

    /**
     * Produces method with return value {@link Subject} with one of {@link RxSubject} modifiers.
     * <p>
     * Required signature:
     * <ul>
     * <li><strong>Provide assignable for {@link Observer} first parameter in target method</strong></li>
     * </ul>
     * </p>
     *
     * @see DefRxSubject declare parse by this factory
     * @see RxSubject modifyers
     */
    public static RxMethod<DefRxSubject>
    createRxSubjectMethod(@NonNull final MethodDefinition<DefRxSubject> methodDefinition,
                          final int methodId,
                          @NonNull final ExecutableElement rawMethod,
                          @NonNull final Types types,
                          @NonNull final Elements elements) throws MethodSignatureException {
        return new RxMethod<>(methodDefinition,
                              methodId,
                              createName(rawMethod, MethodSuffix.RX_SUBJECT),
                              rawMethod,
                              Observable.class,
                              getSubjectType(rawMethod),
                              types,
                              elements,
                              TypeUtils.filterTypeMirror(methodDefinition.getRawDefinition()::value));

    }

    private RxMethod(@NonNull final MethodDefinition<DefinitionType> methodDefinition,
             final int methodId,
             @NonNull String methodName,
             @NonNull final ExecutableElement rawMethod,
             Class<?> observableType,
             Class<?> observerType,
             Types typesUtils,
             Elements elementsUtils,
             TypeMirror resultType) throws MethodSignatureException {
        super(methodDefinition, methodId, methodName, rawMethod);
        this.observerType = observerType;
        this.resultType = TypeUtils.protectType(typesUtils, resultType);
        parameters = validateAndGetParams(typesUtils, elementsUtils);
        observableTypeName = ParameterizedTypeName.get(ClassName.get(observableType), TypeName.get(this.resultType));
        observerTypeName = ParameterizedTypeName.get(ClassName.get(observerType), TypeName.get(this.resultType));
        create();
    }

    @Override
    protected void createSignature(@NonNull MethodSpec.Builder builder) {
        super.createSignature(builder);
        builder.returns(observableTypeName).addParameters(TypeUtils.getParameters(parameters));
    }

    @Override
    protected CodeBlock createCode(@NonNull MethodSpec.Builder builder) {
        return CodeBlock.builder()
                        // Wrap to Transport
                        .addStatement("$T $L = $T.create()",
                                      observerTypeName,
                                      VALUE_NAME_PUBLISHER,
                                      observerType)
                        .addStatement("$T $L = $N.hide()",
                                      observableTypeName, VALUE_NAME_TRANSPORT, VALUE_NAME_PUBLISHER)
                        .build();
    }

    private List<? extends VariableElement> validateAndGetParams(@NonNull Types typesUtils, @NonNull Elements elementsUtils)
    throws MethodSignatureException {
        if(resultType.getKind() == TypeKind.VOID) {
            throw new MethodSignatureException("Invalid value type \"void\"");
        }

        final List<? extends VariableElement> parameters = getRawMethod().getParameters();

        final TypeMirror publisher = suggestPublisher(resultType, typesUtils, elementsUtils);

        if(parameters.isEmpty() || !typesUtils.isAssignable(publisher,
                                                            parameters.get(0)
                                                                      .asType())) {
            throw new MethodSignatureException("First parameter must accept " + publisher);
        }

        return parameters.size() == 1 ? Collections.emptyList() : parameters.subList(1, parameters.size());
    }

    private TypeMirror suggestPublisher(@NonNull TypeMirror resultType, @NonNull Types typesUtils, @NonNull Elements elementsUtils) {
        final TypeElement publisher = elementsUtils.getTypeElement(observerType.getCanonicalName());
        return typesUtils.getDeclaredType(publisher, resultType);
    }

    private static Class<? extends Subject> getSubjectType(ExecutableElement method) {
        final RxSubject subject = method.getAnnotation(DefRxSubject.class)
                                        .subject();
        switch(subject) {
            case ASYNC: {
                return AsyncSubject.class;
            }
            case REPLAY: {
                return ReplaySubject.class;
            }
            case PUBLISH: {
                return PublishSubject.class;
            }
            case BEHAVIOUR: {
                return BehaviorSubject.class;
            }
            case UNICAST: {
                return UnicastSubject.class;
            }
            default:
                throw new IllegalStateException("Unknown subject: " + subject);
        }
    }

    private final Class<?> observerType;
    private final TypeMirror resultType;
    private final ParameterizedTypeName observableTypeName;
    private final ParameterizedTypeName observerTypeName;
    private final List<? extends VariableElement> parameters;
}
