package com.imkorn.exy.compiler.exceptions;

public class RouterDeclarationException extends MapperBuildException {

    public RouterDeclarationException(String s) {
        super(s);
    }
}
