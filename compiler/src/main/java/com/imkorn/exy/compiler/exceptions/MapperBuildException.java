package com.imkorn.exy.compiler.exceptions;

public class MapperBuildException extends Exception {
    public MapperBuildException(String s) {
        super(s);
    }
}
