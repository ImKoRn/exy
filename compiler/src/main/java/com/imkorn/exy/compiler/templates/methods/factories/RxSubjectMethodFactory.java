package com.imkorn.exy.compiler.templates.methods.factories;

import androidx.annotation.NonNull;

import com.imkorn.exy.annotations.methods.rx.DefRxSubject;
import com.imkorn.exy.compiler.exceptions.MethodSignatureException;
import com.imkorn.exy.compiler.templates.methods.rx.RxMethod;

import javax.lang.model.element.ExecutableElement;
import javax.lang.model.util.Elements;
import javax.lang.model.util.Types;

public final class RxSubjectMethodFactory extends MethodFactory<DefRxSubject, RxMethod<DefRxSubject>> {

    public RxSubjectMethodFactory(@NonNull final Elements elements, @NonNull final Types types) {
        super(DefRxSubject.class);
        this.elements = elements;
        this.types = types;
    }

    @Override
    public RxMethod<DefRxSubject> build(int methodId, @NonNull ExecutableElement rawMethod) throws MethodSignatureException {
        return RxMethod.createRxSubjectMethod(getMethodDefinition(rawMethod), methodId, rawMethod, types, elements);
    }

    @NonNull
    private final Elements elements;
    @NonNull
    private final Types types;
}
