package com.imkorn.exy.compiler.utilities.definitions;

import com.imkorn.exy.annotations.ApiAccess;
import com.imkorn.exy.annotations.methods.rx.DefRxSingle;

final class MethodDefinitionRxSingle extends MethodDefinition<DefRxSingle> {

    MethodDefinitionRxSingle(DefRxSingle rawDefinition) {
        super(rawDefinition);
    }

    @Override
    public ApiAccess[] getApiAccess() {
        return rawDefinition.access();
    }

}
