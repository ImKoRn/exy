package com.imkorn.exy.compiler.templates.methods;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.imkorn.exy.compiler.utilities.definitions.MethodDefinition;
import com.imkorn.exy.mapping.Api;
import com.squareup.javapoet.MethodSpec;

import java.lang.annotation.Annotation;

import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.Modifier;

/**
 * Default api generation logic
 */
public abstract class Method<DefinitionType extends Annotation> {

    public final MethodDefinition<DefinitionType> getMethodDefinition() {
        return methodDefinition;
    }

    public final int getMethodId() {
        return methodId;
    }

    public final MethodSpec getMethod() {

        if(method == null) {
            throw new IllegalStateException("Method#create not invoked");
        }

        return method;
    }

    @NonNull
    public final ExecutableElement getRawMethod() {
        return rawMethod;
    }

    Method(@NonNull final MethodDefinition<DefinitionType> methodDefinition,
           final int methodId,
           @NonNull String methodName,
           @NonNull final ExecutableElement rawMethod) {
        this.methodDefinition = methodDefinition;
        this.methodId = methodId;
        this.rawMethod = rawMethod;
        this.methodName = methodName;
    }

    protected final void create() {
        final MethodSpec.Builder builder = MethodSpec.methodBuilder(methodName);
        createSignature(builder);
        createBody(builder);
        builder.varargs(rawMethod.isVarArgs());
        method = builder.build();
    }

    protected void createSignature(@NonNull MethodSpec.Builder builder) {
        builder.addJavadoc("Based on {@link $T#$L}", rawMethod.getEnclosingElement(), rawMethod)
               .addModifiers(Modifier.FINAL, Modifier.PUBLIC);
    }

    @NonNull
    protected static String createName(@NonNull ExecutableElement element, @Nullable String suffix) {
        return suffix != null ? element.getSimpleName() + suffix : element.getSimpleName().toString();
    }

    protected abstract void createBody(@NonNull MethodSpec.Builder builder);

    private final MethodDefinition<DefinitionType> methodDefinition;

    private final int methodId;

    private final ExecutableElement rawMethod;

    private final String methodName;

    private MethodSpec method;

    /**
     * @see Api#invoke(int, Object)
     */
    static final String METHOD_INVOKE = "invoke";
    static final String VALUE_NAME_ARGS = "exy_Args";
}
