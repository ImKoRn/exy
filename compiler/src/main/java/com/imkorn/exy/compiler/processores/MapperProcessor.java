package com.imkorn.exy.compiler.processores;

import com.imkorn.exy.annotations.DefMappable;
import com.imkorn.exy.compiler.exceptions.MapperBuildException;
import com.imkorn.exy.compiler.templates.MapperBuilder;
import com.imkorn.exy.compiler.templates.methods.factories.AsyncMethodFactory;
import com.imkorn.exy.compiler.templates.methods.factories.MethodFactory;
import com.imkorn.exy.compiler.templates.methods.factories.RxMaybeMethodFactory;
import com.imkorn.exy.compiler.templates.methods.factories.RxSingleMethodFactory;
import com.imkorn.exy.compiler.templates.methods.factories.RxSubjectMethodFactory;
import com.imkorn.exy.compiler.exceptions.MethodSignatureException;
import com.imkorn.exy.compiler.utilities.CollectUtils;
import com.squareup.javapoet.JavaFile;

import java.io.IOException;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.Set;

import javax.annotation.processing.AbstractProcessor;
import javax.annotation.processing.Filer;
import javax.annotation.processing.Messager;
import javax.annotation.processing.ProcessingEnvironment;
import javax.annotation.processing.RoundEnvironment;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.TypeElement;
import javax.lang.model.util.Elements;
import javax.lang.model.util.Types;
import javax.tools.Diagnostic;

public class MapperProcessor extends AbstractProcessor {

    @Override
    public synchronized void init(ProcessingEnvironment processingEnvironment) {
        super.init(processingEnvironment);
        messager = processingEnvironment.getMessager();
        filer = processingEnvironment.getFiler();

        elementsUtils = processingEnvironment.getElementUtils();
        typesUtils = processingEnvironment.getTypeUtils();

        // Factories
        methodTemplatesBuilders.add(new AsyncMethodFactory());
        methodTemplatesBuilders.add(new RxMaybeMethodFactory(elementsUtils, typesUtils));
        methodTemplatesBuilders.add(new RxSingleMethodFactory(elementsUtils, typesUtils));
        methodTemplatesBuilders.add(new RxSubjectMethodFactory(elementsUtils, typesUtils));
    }

    @Override
    public boolean process(Set<? extends TypeElement> set, RoundEnvironment roundEnvironment) {
        processMappable(roundEnvironment);
        return true;
    }

    private void processMappable(RoundEnvironment roundEnvironment) {
        for(Element element : roundEnvironment.getElementsAnnotatedWith(DefMappable.class)) {
            // Check precondition
            if(element.getKind() != ElementKind.CLASS) {
                continue;
            }

            final TypeElement mappable = (TypeElement) element;
            final DefMappable mappableConfig = element.getAnnotation(DefMappable.class);

            // If valid save as class
            boolean valid = true;

            final MapperBuilder builder = new MapperBuilder(typesUtils, mappable, mappableConfig);

            final Collection<ExecutableElement> methods = CollectUtils.collectMethods(typesUtils, elementsUtils, mappable, mappableConfig);

            for(ExecutableElement method : methods) {
                for(MethodFactory methodFactory : methodTemplatesBuilders) {

                    if(methodFactory.isAcceptable(method)) {
                        try {
                            builder.addMethod(method, methodFactory);
                        } catch(MethodSignatureException e) {
                            messager.printMessage(Diagnostic.Kind.ERROR, e.getMessage(), method);
                            valid = false;
                        }
                    }
                }
            }

            if(!valid) {
                continue;
            }

            final String packageName = elementsUtils.getPackageOf(element)
                                                    .getQualifiedName()
                                                    .toString();

            try {
                JavaFile javaFile = JavaFile.builder(packageName, builder.build(packageName)).build();
                javaFile.writeTo(filer);
            } catch(IOException | MapperBuildException e) {
                messager.printMessage(Diagnostic.Kind.ERROR, e.getMessage(), element);
                e.printStackTrace();
            }
        }
    }

    @Override
    public SourceVersion getSupportedSourceVersion() {
        return SourceVersion.latestSupported();
    }

    @Override
    public Set<String> getSupportedAnnotationTypes() {
        return Collections.singleton(DefMappable.class.getCanonicalName());
    }

    // Utils
    private Messager messager;
    private Filer filer;
    private Types typesUtils;
    private Elements elementsUtils;

    // Mapper processing
    private Collection<MethodFactory> methodTemplatesBuilders = new LinkedList<>();
}
