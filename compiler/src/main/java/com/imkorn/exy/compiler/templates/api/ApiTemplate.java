package com.imkorn.exy.compiler.templates.api;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.imkorn.exy.mapping.Api;
import com.imkorn.exy.mapping.ApiInterceptor;
import com.imkorn.exy.mapping.Mapper;
import com.imkorn.exy.mapping.RouteApi;
import com.squareup.javapoet.ClassName;
import com.squareup.javapoet.MethodSpec;
import com.squareup.javapoet.ParameterSpec;
import com.squareup.javapoet.ParameterizedTypeName;
import com.squareup.javapoet.TypeName;
import com.squareup.javapoet.TypeSpec;
import com.squareup.javapoet.WildcardTypeName;

import java.util.Collection;

import javax.lang.model.element.Modifier;

/**
 * Builds basic {@link Api} for {@link Mapper}
 */
public class ApiTemplate {

    public static ApiTemplate createPublic(@NonNull final ClassName mapperType,
                                           @Nullable String deployPackage,
                                           @NonNull Collection<MethodSpec> methods) {
        return new ApiTemplate(PUBLIC_NAME, METHOD_NAME_CREATE_PUBLIC_API,
                               mapperType,
                               deployPackage,
                               methods);
    }

    public static ApiTemplate createChild(@NonNull final ClassName mapperType,
                                          @Nullable String deployPackage,
                                          @NonNull Collection<MethodSpec> methods) {
        return new ApiTemplate(CHILD_NAME, METHOD_NAME_CREATE_CHILD_API,
                               mapperType,
                               deployPackage,
                               methods);
    }

    public static ApiTemplate createSelf(@NonNull final ClassName mapperType,
                                         @Nullable String deployPackage,
                                         @NonNull Collection<MethodSpec> methods) {
        return new ApiTemplate(SELF_NAME, METHOD_NAME_CREATE_SELF_API,
                               mapperType,
                               deployPackage,
                               methods);
    }

    public static TypeSpec createRoute(@NonNull final ClassName mapperType, @NonNull final Collection<MethodSpec> methods) {
        return buildType(mapperType, ROUTE_SUFFIX, methods, true);
    }

    public final TypeSpec build() {
        return buildType(mapperType, name, methods, false);
    }

    public final MethodSpec buildCreateMethod() {
        return MethodSpec.methodBuilder(createTypeName)
                         .addAnnotation(NonNull.class)
                         .addModifiers(Modifier.PUBLIC, Modifier.FINAL)
                         .addParameter(TypeName.INT, PARAMETER_ID, Modifier.FINAL)
                         .addParameter(ParameterSpec.builder(ApiInterceptor.class, PARAMETER_INTERCEPTOR, Modifier.FINAL)
                                                    .addAnnotation(NonNull.class)
                                                    .build())
                         .addStatement("return new $T($L, $L)", typeName, PARAMETER_ID, PARAMETER_INTERCEPTOR)
                         .returns(typeName)
                         .build();
    }

    public TypeName getTypeName() {
        return typeName;
    }

    Collection<MethodSpec> getMethods() {
        return methods;
    }

    private ApiTemplate(@Nullable final String name,
                        @NonNull final String createMethodName,
                        @NonNull final ClassName mapperType,
                        @Nullable final String deployPackage,
                        @NonNull final Collection<MethodSpec> methods) {
        this.mapperType = mapperType;
        this.methods = methods;
        this.createTypeName = createMethodName;
        this.name = name;
        this.typeName = ClassName.get(deployPackage, this.name);
    }

    private static TypeSpec buildType(@NonNull final ClassName mapperType,
                                      @NonNull final String name,
                                      @NonNull final Collection<MethodSpec> methods,
                                      final boolean route) {
        final MethodSpec constructor = MethodSpec.constructorBuilder()
                                                 .addModifiers(route? Modifier.PUBLIC : Modifier.PRIVATE)
                                                 .addParameter(TypeName.INT, PARAMETER_ID, Modifier.FINAL)
                                                 .addParameter(ParameterSpec.builder(ApiInterceptor.class, PARAMETER_INTERCEPTOR, Modifier.FINAL)
                                                                            .addAnnotation(NonNull.class)
                                                                            .build())
                                                 .addStatement("super($L, $L)",
                                                               PARAMETER_ID,
                                                               PARAMETER_INTERCEPTOR)
                                                 .build();

        final TypeSpec.Builder typeBuilder = TypeSpec.classBuilder(name);

        if(route) {
            typeBuilder.addModifiers(Modifier.STATIC);
        }

        final WildcardTypeName wildcardTypeName = WildcardTypeName.subtypeOf(Object.class);
        final ParameterizedTypeName rawMapper = ParameterizedTypeName.get(ClassName.get(Mapper.class),
                                                                          wildcardTypeName,
                                                                          wildcardTypeName,
                                                                          wildcardTypeName,
                                                                          wildcardTypeName);

        return typeBuilder.superclass(ClassName.get(route? RouteApi.class : Api.class))
                       .addModifiers(Modifier.PUBLIC, Modifier.FINAL)
                       .addMethod(constructor)
                       .addMethod(MethodSpec.methodBuilder(METHOD_NAME_GET_MAPPER)
                                            .addModifiers(Modifier.PUBLIC, Modifier.FINAL)
                                            .addAnnotation(Override.class)
                                            .addStatement("return $T.class", mapperType)
                                            .returns(ParameterizedTypeName.get(ClassName.get(Class.class),
                                                                               WildcardTypeName.subtypeOf(rawMapper)))
                                            .build())
                       .addMethods(methods)
                       .build();
    }

    private final String name;
    private final Collection<MethodSpec> methods;
    private final String createTypeName;
    private final TypeName typeName;
    private final ClassName mapperType;

    private static final String PUBLIC_NAME = "Api";
    private static final String CHILD_NAME = "ParentApi";
    private static final String SELF_NAME = "SelfApi";
    private static final String ROUTE_SUFFIX = "RouteApi";
    private static final String METHOD_NAME_CREATE_PUBLIC_API = "createPublicApi";
    private static final String METHOD_NAME_CREATE_CHILD_API = "createChildApi";
    private static final String METHOD_NAME_CREATE_SELF_API = "createSelfApi";
    private static final String METHOD_NAME_GET_MAPPER = "getMapper";

    private static final String PARAMETER_ID = "id";
    private static final String PARAMETER_INTERCEPTOR = "interceptor";
}
