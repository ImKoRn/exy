package com.imkorn.exy.compiler.utilities.definitions;

import com.imkorn.exy.annotations.ApiAccess;
import com.imkorn.exy.annotations.methods.DefAsync;

final class MethodDefinitionAsync extends MethodDefinition<DefAsync> {

    MethodDefinitionAsync(DefAsync rawDefinition) {
        super(rawDefinition);
    }

    @Override
    public ApiAccess[] getApiAccess() {
        return rawDefinition.access();
    }

}
